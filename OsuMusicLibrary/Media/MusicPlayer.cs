﻿using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Threading;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Types;
using OsuMusicLibrary.GUI;

namespace OsuMusicLibrary.Media
{
	class MusicPlayer
	{
		public static MediaPlayer Player { get; private set; }
		private static readonly DispatcherTimer timer;

        public static bool Playing { get; private set; }
        private static TimeSpan startPosition;

		// The file path used for temporary audio files that are used for songs with paths can't be used as a Uri:s
		private const String tempAudioFilePath = "_TempAudioFile_.mp3";
	
		static MusicPlayer()
		{
            Playing = false;

			Player = new MediaPlayer();
			Player.MediaEnded += OnAudioStopped;
			Player.MediaFailed += OnAudioStopped;
            Player.MediaOpened += OnAudioOpened;
            Player.Volume = 0.8;

			timer = new DispatcherTimer {Interval = TimeSpan.FromSeconds(1)};
			timer.Tick += TimerTick;
			timer.Start();

			// Wipe the temp file when the program closes
			App.Instance.Exit += (sender, e) => TryWipeTempFile();
		}

		public static void PlaySong(SongContext context, SongData song)
		{
			// Make sure the song is valid before updating the context
			if (song == null || !File.Exists(song.AudioFilePath)) return;

			SongPicker.SetNewContext(context, song);
			loadSong(song);
			Play();
		}

		public static void PlayFromCurrentCategory()
		{
			SongPicker.SetNewContext(GlobalData.Session.CurrentCategory != null ? SongContext.CATEGORY : SongContext.ALL_SONGS);
			loadSong(SongPicker.CurrentSong);
			Play();
		}

		private static void loadSong(SongData song)
		{
			// Make sure the song is valid
			if (song == null || !File.Exists(song.AudioFilePath)) return;

			Uri uri = new Uri(song.AudioFilePath);

			// Load a new song			
			if (File.Exists(uri.LocalPath))
			{
				// If the uri is fine, load the song and wipe any existing temp file
				Player.Open(uri);
				TryWipeTempFile();
			}
			else
			{
				// If the file name fails to be turned into a uri, copy it to a temp file and play that instead.
				File.Copy(song.AudioFilePath, tempAudioFilePath, true);
				Player.Open(new Uri(tempAudioFilePath, UriKind.Relative));
			}

			// Update the current position in the song to either the preview position or the beginning, depending on the settings
			startPosition = new TimeSpan(0, 0, 0, 0, GlobalData.Settings.IgnorePreviewTime ? 0 : song.PreviewTime.TotalMilliSeconds);

            // Set the current time if it's already done loading. If it's currently loading, it will be set later by an event.
            if (!Player.IsBuffering) Player.Position = startPosition;

			// Update the descriptions in the GUI
			MainWindow.OMLWindow.Description1.Content = song.DisplayTitle + " by " + song.DisplayArtist;
			MainWindow.OMLWindow.Description2.Content = song.DisplaySource != "" ? song.DisplaySource : "Mapped by " + song.DisplayCreator;
			MainWindow.OMLWindow.Description3.Content = song.DisplaySource != "" ? "Mapped by " + song.DisplayCreator : "";
		}

		/**
		 * If a temp file exists it gets deleted, but otherwise nothing happens.
		 */
		private static void TryWipeTempFile()
		{
			// Wipe any remaining temp files
			if (!File.Exists(tempAudioFilePath)) return;
			try
			{
				File.Delete(tempAudioFilePath);
			}
			catch (Exception) { }
		}

		public static void Play()
		{
			if (Player.Source == null) return;
			
			Player.Play();

			if (SongPicker.CurrentSong != null)
			{
				MainWindow.OMLWindow.Title = SongPicker.CurrentSong.DisplayTitle + " by " + SongPicker.CurrentSong.DisplayArtist + " - Osu Music Library";
			}

			if (!Playing)
			{
				Playing = true;

				// Change the image of the buttons inside the application
				MainWindow.OMLWindow.PlayPause.Content = MainWindow.OMLWindow.FindResource("Pause");

				// Change the thumbnail buttons
				MainWindow.OMLWindow.ThumbnailPlayPause.ImageSource = (ImageSource)MainWindow.OMLWindow.FindResource("BitmapPause");
				MainWindow.OMLWindow.ThumbnailPlayPause.Description = "Pause";
				MainWindow.OMLWindow.ThumbnailPlayPause.Click -= MainWindow.OMLWindow.Thumnail_Play;
				MainWindow.OMLWindow.ThumbnailPlayPause.Click += MainWindow.OMLWindow.Thumnail_Pause;
			}
		}

		public static void Pause()
		{
            if (Player.Source != null)
			{
				Player.Pause();
			}

            MainWindow.OMLWindow.Title = "Osu Music Library";

            if (Playing)
            {
                Playing = false;

                // Change the image of the buttons inside the application
                MainWindow.OMLWindow.PlayPause.Content = MainWindow.OMLWindow.FindResource("Play");

                // Change the thumbnail buttons
                MainWindow.OMLWindow.ThumbnailPlayPause.ImageSource = (ImageSource)MainWindow.OMLWindow.FindResource("BitmapPlay");
                MainWindow.OMLWindow.ThumbnailPlayPause.Description = "Play";
                MainWindow.OMLWindow.ThumbnailPlayPause.Click -= MainWindow.OMLWindow.Thumnail_Pause;
                MainWindow.OMLWindow.ThumbnailPlayPause.Click += MainWindow.OMLWindow.Thumnail_Play;
            }
		}

        public static void Stop()
        {
            if (SongPicker.CurrentSong != null)
            {
                Player.Position = new TimeSpan(0, 0, 0, 0, GlobalData.Settings.IgnorePreviewTime ? 0 : SongPicker.CurrentSong.PreviewTime.TotalMilliSeconds);
            }
            Pause();
        }

		public static void PlayNextSong()
		{
			SongData song = SongPicker.NextSong();
			if (song != null)
			{
				loadSong(song);
				Play();
			}
			else
			{
				Pause();
			}
		}

		public static void PlayPreviousSong()
		{
			SongData song = SongPicker.PreviousSong();
			if (song != null)
			{
				loadSong(song);
				Play();
			}
			else
            {
                Stop();
			}
		}

		/**
		 * Set the position in the song measured as a fraction of the whole song.
		 */
		public static void SetTime(double fraction)
		{
			if (Player.Source != null && Player.NaturalDuration.HasTimeSpan)
			{
				Player.Position = new TimeSpan(0, 0, 0, 0, (int)(Player.NaturalDuration.TimeSpan.TotalMilliseconds * fraction));
				Play();
			}
		}

        /**
		 * Set the player to the correct position in the song when it's completely loaded
		 */
        private static void OnAudioOpened(object sender, EventArgs e)
        {
            Player.Position = startPosition;
        }

		/**
		 * Display the play button if the audio stopped
		 */
		private static void OnAudioStopped(object sender, EventArgs e)
		{
			// If the song ended before it started, stop
			if (Player.Position == new TimeSpan(0))
			{
				Stop();
				return;
			}

			if (GlobalData.Settings.SwitchSongs)
			{
				PlayNextSong();
			}
			else
			{
				if (GlobalData.Settings.AutoRepeat)
				{
					Player.Position = new TimeSpan();
				}
				else
				{
					Pause();
				}
			}
		}

		public static void StopTimer()
		{
			timer.Stop();
		}

		public static void StartTimer()
		{
			timer.Start();
		}

		/**
		 * Updates the text for the song time once a second.
		 */
		private static void TimerTick(object sender, EventArgs e)
		{
			MainWindow window = MainWindow.OMLWindow;
			if (window == null) return;
			if (Player.Source != null && Player.NaturalDuration.HasTimeSpan)
			{
				try
				{
					window.CurrentPlayTime.Content = Player.Position.ToString(@"mm\:ss");
					window.AudioLength.Content = Player.NaturalDuration.TimeSpan.ToString(@"mm\:ss");
					window.SetSliderPosition(Player.Position.TotalMilliseconds / Player.NaturalDuration.TimeSpan.TotalMilliseconds);
				}
				catch (InvalidOperationException) 
				{
					Console.WriteLine("Why is this happening?!");
				}
			}
			else
			{
				window.CurrentPlayTime.Content = "--:--";
				window.AudioLength.Content = "--:--";
			}
		}
	}
}
