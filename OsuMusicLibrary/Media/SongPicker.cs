﻿using System;
using System.Collections.Generic;
using System.Linq;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Types;
using OsuMusicLibrary.Data.Util;

namespace OsuMusicLibrary.Media
{
	public class SongPicker
	{
        public static SongData CurrentSong { get; private set; }
		private static SongContext context = SongContext.ALL_SONGS;
		private static Category category = null;
		private static readonly List<SongData> SongHistory = new List<SongData>();
		private static int currentHistoryIndex = 0;

        public SongPicker()
        {
            CurrentSong = null;
        }

		public static void SetNewContext(SongContext newContext, SongData song = null)
		{
			// Remove all songs in front of this song in the song history
			for (int i = currentHistoryIndex - 1; i >= 0; i--)
			{
				SongHistory.RemoveAt(i);
			}
			
			// Reset the index to the first position
			currentHistoryIndex = 0;

			// Update the context
			context = newContext;
			category = GlobalData.Session.CurrentCategory;

			// Update the current song
			CurrentSong = song;

			// If no song was supplied, try to pick one
			if (song == null) CurrentSong = song = PickNewSong();

			// Add the song to the song history
			if (song != null) SongHistory.Insert(0, song);
		}

		public static SongData PreviousSong()
		{
			if (currentHistoryIndex + 1 < SongHistory.Count)
			{
                // If there are still songs behind the current one in the history, play the next one in turn
				currentHistoryIndex++;
                SongData song = SongHistory[currentHistoryIndex];
                CurrentSong = song;

                return song;
			}
			else
			{
                // If this is the last song in the history, play nothing
				return null;
			}
		}

		public static SongData NextSong()
		{
			if (currentHistoryIndex > 0)
			{
				// If not at the front of the history list, go one step forward and play the predetermined song
				currentHistoryIndex--;
				return CurrentSong = SongHistory[currentHistoryIndex];
			}
			else
			{
				// If at the front of the history list, pick a new song from the current context
                SongData song = PickNewSong();

				// Add the selected song to the song history
                if (song != null) SongHistory.Insert(0, song);

                // Update the current song
                CurrentSong = song;

                return song;
			}
		}

        /**
         * Pick a new song by calling the appropriate song picking function based on the context.
         */
        public static SongData PickNewSong()
        {
            switch (context)
	        {
		        case SongContext.ALL_SONGS:
		        {
			        if (GlobalData.Settings.Shuffle)
			        {
				        return PickRandomSong(SongListHelper.FilterList(GlobalData.ObservableSongList, GlobalData.Session.AllSongsFilter));
			        }
			        else
			        {
				        return PickFollowingSongFiltered(GlobalData.ObservableSongList, GlobalData.Session.AllSongsFilter, GlobalData.Session.AllSongsSort);
			        }
		        }
		        case SongContext.CATEGORY:
		        {
			        if (GlobalData.Settings.Shuffle)
			        {
				        // Pick a random song from the songs that passed the filter
				        return PickRandomSong(SongListHelper.FilterList(SongListHelper.SongsFromIDs(category.SongIDs), category.Filter));
			        }
			        else
			        {
				        // Pick the next song based on the current sorting and only from songs that passed the filter
				        return PickFollowingSongFiltered(SongListHelper.SongsFromIDs(category.SongIDs), category.Filter, category.Sort);
			        }
		        }
		        case SongContext.QUEUE:
		        {
			        if (GlobalData.Settings.Shuffle)
			        {
				        return PickRandomSong(GlobalData.Session.Queue);
			        }
			        else
			        {
				        return PickFollowingSong(GlobalData.Session.Queue);
			        }
		        }
				default:
		        {
					// If this happened, something went wrong
					return null;
		        }
	        }
        }

        /**
         * Pick a random song from a list, excluding the current song unless there is exactly one song in the list.
         */
        public static SongData PickRandomSong(List<SongData> songs)
        {
            if (songs.Count == 0) return null;      // If there aren't any songs, play nothing
            if (songs.Count == 1) return songs[0];  // If there is only one song available, repeat it

            Random random = new Random();

            int currentIndex = songs.IndexOf(CurrentSong);

            // Select a random song, but skip the current song if it exists in the list
            if (currentIndex != -1)
            {
                int index = random.Next(songs.Count - 1);
                return songs[index < songs.IndexOf(CurrentSong) ? index : (index + 1)];
            }
            else
            {
                return songs[random.Next(songs.Count)];
            }
        }

        /**
         * Pick the following song from a list, or nothing if is is empty
         */
        public static SongData PickFollowingSong(List<SongData> songs)
        {
            // If there are no songs, play nothing
            if (songs.Count == 0) return null;

            // Get the next song in the queue, or the first one if the current song was removed.
            return songs[(songs.IndexOf(CurrentSong) + 1) % songs.Count];
        }

        /**
         * Pick the following song in turn from a list that is sorted and filtered the same way as the GUI.
         */
        public static SongData PickFollowingSongFiltered(IEnumerable<SongData> list, Filter filter, SortDirective sort)
        {
            // Sort the list of songs, either ascending or descending
			List<SongData> sortedList = SongListHelper.SortList(list, sort);

            // Add all the songs that passed the filter to a hashset for quick access
			HashSet<SongData> visibleSongs = new HashSet<SongData>();
            foreach (SongData song in sortedList.Where(data => filter.IsVisible(data)))
            {
	            visibleSongs.Add(song);
            }

            // If no songs are available to pick from, play nothing
            if (visibleSongs.Count == 0) return null;

            // Pick the next song and see if it's available and repeat until one is found.
            SongData newSong;
            int index = sortedList.IndexOf(CurrentSong); // IndexOf returns -1 if not found. In that case, 0 will be tried first.
            do
            {
                index = (index + 1) % sortedList.Count;
                newSong = sortedList[index];
            }
            while (!visibleSongs.Contains(newSong));

            return newSong;
        }
	}
}
