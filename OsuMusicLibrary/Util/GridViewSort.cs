﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using OsuMusicLibrary.GUI;

namespace Wpf.Util
{
	public class GridViewSort
	{
		#region Public attached properties

		public static ICommand GetCommand(DependencyObject obj)
		{
			return (ICommand)obj.GetValue(CommandProperty);
		}

		public static void SetCommand(DependencyObject obj, ICommand value)
		{
			obj.SetValue(CommandProperty, value);
		}

		// Using a DependencyProperty as the backing store for Command.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty CommandProperty =
			DependencyProperty.RegisterAttached(
				"Command",
				typeof(ICommand),
				typeof(GridViewSort),
				new UIPropertyMetadata(
					null,
					(o, e) =>
					{
						ItemsControl listView = o as ItemsControl;
						if (listView != null)
						{
							if (!GetAutoSort(listView)) // Don't change click handler if AutoSort enabled
							{
								if (e.OldValue != null && e.NewValue == null)
								{
									listView.RemoveHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(ColumnHeader_Click));
								}
								if (e.OldValue == null && e.NewValue != null)
								{
									listView.AddHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(ColumnHeader_Click));
								}
							}
						}
					}
				)
			);

		public static bool GetAutoSort(DependencyObject obj)
		{
			return (bool)obj.GetValue(AutoSortProperty);
		}

		public static void SetAutoSort(DependencyObject obj, bool value)
		{
			obj.SetValue(AutoSortProperty, value);
		}

		// Using a DependencyProperty as the backing store for AutoSort.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty AutoSortProperty =
			DependencyProperty.RegisterAttached(
				"AutoSort",
				typeof(bool),
				typeof(GridViewSort),
				new UIPropertyMetadata(
					false,
					(o, e) =>
					{
						ListView listView = o as ListView;
						if (listView != null)
						{
							if (GetCommand(listView) == null) // Don't change click handler if a command is set
							{
								bool oldValue = (bool)e.OldValue;
								bool newValue = (bool)e.NewValue;
								if (oldValue && !newValue)
								{
									listView.RemoveHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(ColumnHeader_Click));
								}
								if (!oldValue && newValue)
								{
									listView.AddHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(ColumnHeader_Click));
								}
							}
						}
					}
				)
			);

		public static string GetPropertyName(DependencyObject obj)
		{
			return (string)obj.GetValue(PropertyNameProperty);
		}

		public static void SetPropertyName(DependencyObject obj, string value)
		{
			obj.SetValue(PropertyNameProperty, value);
		}

		// Using a DependencyProperty as the backing store for PropertyName.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PropertyNameProperty =
			DependencyProperty.RegisterAttached(
				"PropertyName",
				typeof(string),
				typeof(GridViewSort),
				new UIPropertyMetadata(null)
			);

		public static bool GetShowSortGlyph(DependencyObject obj)
		{
			return (bool)obj.GetValue(ShowSortGlyphProperty);
		}

		public static void SetShowSortGlyph(DependencyObject obj, bool value)
		{
			obj.SetValue(ShowSortGlyphProperty, value);
		}

		// Using a DependencyProperty as the backing store for ShowSortGlyph.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ShowSortGlyphProperty =
			DependencyProperty.RegisterAttached("ShowSortGlyph", typeof(bool), typeof(GridViewSort), new UIPropertyMetadata(true));

		public static ImageSource GetSortGlyphAscending(DependencyObject obj)
		{
			return (ImageSource)obj.GetValue(SortGlyphAscendingProperty);
		}

		public static void SetSortGlyphAscending(DependencyObject obj, ImageSource value)
		{
			obj.SetValue(SortGlyphAscendingProperty, value);
		}

		// Using a DependencyProperty as the backing store for SortGlyphAscending.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SortGlyphAscendingProperty =
			DependencyProperty.RegisterAttached("SortGlyphAscending", typeof(ImageSource), typeof(GridViewSort), new UIPropertyMetadata(null));

		public static ImageSource GetSortGlyphDescending(DependencyObject obj)
		{
			return (ImageSource)obj.GetValue(SortGlyphDescendingProperty);
		}

		public static void SetSortGlyphDescending(DependencyObject obj, ImageSource value)
		{
			obj.SetValue(SortGlyphDescendingProperty, value);
		}

		// Using a DependencyProperty as the backing store for SortGlyphDescending.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SortGlyphDescendingProperty =
			DependencyProperty.RegisterAttached("SortGlyphDescending", typeof(ImageSource), typeof(GridViewSort), new UIPropertyMetadata(null));

		#endregion

		#region Private attached properties

		private static GridViewColumnHeader GetSortedColumnHeader(DependencyObject obj)
		{
			return (GridViewColumnHeader)obj.GetValue(SortedColumnHeaderProperty);
		}

		private static void SetSortedColumnHeader(DependencyObject obj, GridViewColumnHeader value)
		{
			obj.SetValue(SortedColumnHeaderProperty, value);
		}

		// Using a DependencyProperty as the backing store for SortedColumn.  This enables animation, styling, binding, etc...
		private static readonly DependencyProperty SortedColumnHeaderProperty =
			DependencyProperty.RegisterAttached("SortedColumnHeader", typeof(GridViewColumnHeader), typeof(GridViewSort), new UIPropertyMetadata(null));

		#endregion

		#region Column header click event handler

		private static void ColumnHeader_Click(object sender, RoutedEventArgs e)
		{
		    // Removed
		}

		#endregion

		#region Helper methods

        public static void UpdateColumnSort(string propertyName, bool ascending)
        {
			if (!string.IsNullOrEmpty(propertyName))
			{
                if (MainWindow.OMLWindow == null) return;
                GridViewColumnCollection columns = MainWindow.OMLWindow.GridView.Columns;
                GridViewColumn selectedColumn = null;
                foreach (GridViewColumn column in columns)
                {
                    if (GetPropertyName(column) == propertyName)
                    {
                        selectedColumn = column;
                    }
                }

                GridViewColumnHeader headerClicked = null;
                if (selectedColumn != null) headerClicked = selectedColumn.Header as GridViewColumnHeader;

                var listView = MainWindow.OMLWindow.SongList;
				if (listView != null)
				{
					ICommand command = GetCommand(listView);
					if (command != null)
					{
						if (command.CanExecute(propertyName))
						{
							command.Execute(propertyName);
						}
					}
					else if (GetAutoSort(listView))
					{
						ApplySort(listView.Items, propertyName, listView, headerClicked, ascending);
					}
				}
			}
        }

		public static T GetAncestor<T>(DependencyObject reference) where T : DependencyObject
		{
			DependencyObject parent = VisualTreeHelper.GetParent(reference);
			while (!(parent is T))
			{
				parent = VisualTreeHelper.GetParent(parent);
			}
			if (parent != null)
				return (T)parent;
			else
				return null;
		}

        public static void ApplySort(ICollectionView view, string propertyName, ListView listView, GridViewColumnHeader sortedColumnHeader, bool ascending)
		{
			ListSortDirection direction = ListSortDirection.Ascending;
			if (view.SortDescriptions.Count > 0)
			{
				direction = ascending ? ListSortDirection.Ascending : ListSortDirection.Descending;
				
                view.SortDescriptions.Clear();

				GridViewColumnHeader currentSortedColumnHeader = GetSortedColumnHeader(listView);
				if (currentSortedColumnHeader != null)
				{
					RemoveSortGlyph(currentSortedColumnHeader);
				}
			}
			if (!string.IsNullOrEmpty(propertyName))
			{
				view.SortDescriptions.Add(new SortDescription(propertyName, direction));
                view.SortDescriptions.Add(new SortDescription("SongID", direction));
                if (sortedColumnHeader != null && GetShowSortGlyph(listView))
                {
                    AddSortGlyph(sortedColumnHeader, direction, direction == ListSortDirection.Ascending ? GetSortGlyphAscending(listView) : GetSortGlyphDescending(listView));
                }
				SetSortedColumnHeader(listView, sortedColumnHeader);
			}
		}

		private static void AddSortGlyph(GridViewColumnHeader columnHeader, ListSortDirection direction, ImageSource sortGlyph)
		{
			AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(columnHeader);
			adornerLayer.Add(
				new SortGlyphAdorner(
					columnHeader,
					direction,
					sortGlyph
					));
		}

		private static void RemoveSortGlyph(GridViewColumnHeader columnHeader)
		{
			AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(columnHeader);
			Adorner[] adorners = adornerLayer.GetAdorners(columnHeader);
			if (adorners != null)
			{
				foreach (Adorner adorner in adorners)
				{
					if (adorner is SortGlyphAdorner)
						adornerLayer.Remove(adorner);
				}
			}
		}

		#endregion

		#region SortGlyphAdorner nested class


		public class SortGlyphAdorner : Adorner
		{
			private static Geometry ascGeometry =
					Geometry.Parse("M 0 4 L 3.5 0 L 7 4 Z");

			private static Geometry descGeometry =
					Geometry.Parse("M 0 0 L 3.5 4 L 7 0 Z");

			public ListSortDirection Direction { get; private set; }

			public SortGlyphAdorner(UIElement element, ListSortDirection dir, ImageSource sortGlyph)
				: base(element)
			{
				this.Direction = dir;
			}

			protected override void OnRender(DrawingContext DrawingContext)
			{
				base.OnRender(DrawingContext);

				if (AdornedElement.RenderSize.Width < 20) return;

				TranslateTransform transform = new TranslateTransform(AdornedElement.RenderSize.Width - 15, (AdornedElement.RenderSize.Height - 5) / 2);
				DrawingContext.PushTransform(transform);

				Geometry geometry = ascGeometry;
				if (this.Direction == ListSortDirection.Descending) geometry = descGeometry;
				DrawingContext.DrawGeometry(Brushes.Black, null, geometry);
				DrawingContext.Pop();
			}
		}

		#endregion
	}
}