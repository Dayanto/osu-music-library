﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Types;
using OsuMusicLibrary.Data.Util;

namespace OsuMusicLibrary.Data
{
	public class GlobalData
	{
		static GlobalData()
		{
			Settings = null;
			SongCache = null;
			Library = null;

            Session = new Session();
            ObservableSongList = new ObservableCollection<SongData>();
		}

		/** Getter for the settings */
		public static Settings Settings { get; private set; }

		/** Getter for the song cache */
		public static SongCache SongCache { get; private set; }

		/** Getter for the library */
		public static Library Library { get; private set; }

        /** Getter for the current session data */
        public static Session Session { get; private set; }
		
		/** The collection that updates the list view whenever a new item has been added. */
        public static readonly ObservableCollection<SongData> ObservableSongList;

        /** The file name of the settings file. */
        public static String SettingsFilePath { get { return "Settings.json"; } }

        /** The file name of the song cache file. */
        public static String SongCacheFilePath { get { return "SongCache.json"; } }

        /** The file name of the library data file. */
        public static String LibraryFilePath { get { return "Library.json"; } }

		/**
		 * Ignore any previous settings and create a new settings instance.
		 */
		public static void CreateNewSettings()
		{
			Settings = new Settings();
		}

		/**
		 * Load the settings file using Json
		 * 
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 * <exception cref="JsonReaderException">Json deserialization failed</exception>
		 */
		public static void LoadSettings()
		{
			Settings = JsonIO.LoadObject<Settings>(SettingsFilePath);
		}

		/**
		 * Save the settings file using Json
		 * 
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 */
		public static void SaveSettings()
		{
			if (Settings != null)
			{
				JsonIO.SaveObject(SettingsFilePath, Settings, Formatting.Indented);
			}
		}

		/**
		 * Create an empty song cache
		 */
		public static void CreateNewSongCache()
		{
			SongCache = new SongCache();
            ObservableSongList.Clear();
		}

		/**
		 * Load the song cache file using Json
		 * 
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 * <exception cref="JsonReaderException">Json deserialization failed</exception>
		 */
		public static void LoadSongCache()
		{
			SongCache = JsonIO.LoadObject<SongCache>(SongCacheFilePath);
		}

		/**
		 * Save the song cache to file using Json
		 * 
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 */
		public static void SaveSongCache()
		{
			if (SongCache != null)
			{
				JsonIO.SaveObject(SongCacheFilePath, SongCache, Formatting.Indented);
			}
		}

		/**
		 * Ignore any previous data and create a new instance for the library data.
		 */
		public static void CreateNewLibrary()
		{
			Library = new Library();
		}

		/**
		 * Load the library data file using Json
		 * 
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 * <exception cref="JsonReaderException">Json deserialization failed</exception>
		 */
		public static void LoadLibrary()
		{
			Library = JsonIO.LoadObject<Library>(LibraryFilePath);
		}

		/**
		 * Save the library file using Json
		 * 
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 */
		public static void SaveLibrary()
		{
			if (Library != null)
			{
				JsonIO.SaveObject(LibraryFilePath, Library, Formatting.Indented);
			}
		}
	}
}
