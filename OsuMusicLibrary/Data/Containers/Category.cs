﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using OsuMusicLibrary.Data.Types;

namespace OsuMusicLibrary.Data.Containers
{
    [JsonObject(MemberSerialization.OptIn)]
	public class Category : INotifyPropertyChanged
	{
		[JsonProperty]
		public String Name { get { return name; } set { name = value; NotifyPropertyChanged("Name"); } }
		private String name;

		[JsonProperty]
		public HashSet<int> SongIDs;

		public Filter Filter;

		public SortDirective Sort;

		public Category(String name)
		{
			Name = name;
            SongIDs = new HashSet<int>();
            Sort = new SortDirective();
            Filter = new Filter(this);
		}

		public void AddSong(SongData song)
		{
			if (song.IsValid() && !SongIDs.Contains(song.SongID))
			{
				SongIDs.Add(song.SongID);
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyPropertyChanged(String propertyName)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (null != handler)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
