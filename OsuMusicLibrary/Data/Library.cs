﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Types;

namespace OsuMusicLibrary.Data
{
	public class Library
	{
		/** The categories and their data */
		public readonly ObservableCollection<Category> Categories = new ObservableCollection<Category>();

		/** All ratings for the different songs. Ratings aren't directly coupled with the song data, so deleted 
		 *  songs will retain their rating if they're downloaded again later. */
		public readonly SortedList<int, int> Ratings = new SortedList<int, int>();

		/**
		 * Give a song a rating. This will add the song to the list if it doesn's already have a rating. If
		 * the rating is 0 or below, the rating will instead be removed.
		 */
		public void RateSong(SongData song, int rating)
		{
			if (song == null) return;

			if (rating <= 0) RemoveRating(song);

			if (GlobalData.Library.Ratings.ContainsKey(song.SongID))
			{
				GlobalData.Library.Ratings[song.SongID] = rating;
			}
			else
			{
				GlobalData.Library.Ratings.Add(song.SongID, rating);
			}

			GlobalData.SaveLibrary();
		}

		/**
		 * Remove the rating from a song. This will remove it from the list of rated songs
		 */
		public void RemoveRating(SongData song)
		{
			if (song == null) return;

			if (GlobalData.Library.Ratings.ContainsKey(song.SongID))
			{
				GlobalData.Library.Ratings.Remove(song.SongID);
			}

			GlobalData.SaveLibrary();
		}

		/**
		 * Return all the categories a song exists in
		 */
		public List<Category> GetCategoriesOfSong(SongData song)
		{
			return Categories.Where(category => category.SongIDs.Contains(song.SongID)).ToList();
		}

		public Category GetCategory(String name)
		{
			return Categories.FirstOrDefault(category => category.Name == name);
		}

		/**
		 * Whether there exists a category with a given name
		 */
		public bool CategoryExists(String name)
		{
			return Categories.Any(category => category.Name == name);
		}

		/**
		 * Create a new category
		 */
		public void CreateCategory(String name)
		{
			if (name != null && !CategoryExists(name))
			{
				Categories.Add(new Category(name));
				GlobalData.SaveLibrary();
			}
		}

		/**
		 * Create a new category
		 */
		public void CreateCategoryFromQueue(String name)
		{
			if (name != null && !CategoryExists(name))
			{
				Category category = new Category(name);
				foreach (SongData song in GlobalData.Session.Queue)
				{
					category.AddSong(song);
				}
				Categories.Add(category);
				GlobalData.SaveLibrary();
			}
		}

		/**
		 * Rename an existing category
		 */
		public void RenameCategory(Category category, String name)
		{
			if (name != null && !CategoryExists(name))
			{
				category.Name = name;
			}
		}
		
		/**
		 * Remove an existing category entirely
		 */
		public void RemoveCategory(String name)
		{
			if (CategoryExists(name))
			{
				Categories.Remove(GetCategory(name));
				GlobalData.SaveLibrary();
			}
		}
	}
}
