﻿using System;
using System.IO;
using OsuMusicLibrary.Data.Types;

namespace OsuMusicLibrary.Data.Util
{
	public class SongLoader
	{
		/** 
		 *	Load song data from a specified directory. 
		 *	
		 *  <exception cref="InvalidSongException"></exception>
		 */
		public static SongData LoadSongData(String directoryPath, String directoryName)
		{
			// Create an uninitialized song instance
			SongData song = new SongData();

			// Retrieve the song ID, or skip this song if it's invalid
			int id = GetSongID(directoryName);

			if (id <= 0) throw new InvalidSongException("Invalid ID");

			song.SongID = id;
			song.Directory = directoryName;
			
			// Get all files in the song directory
			String[] filepaths;
			try { filepaths = Directory.GetFiles(directoryPath); }
			catch (Exception e) { throw new InvalidSongException("Failed to load file paths: ", e); }
			
			// Parse every .osu file
			foreach (String filepath in filepaths)
			{
				if (Path.GetExtension(filepath) == ".osu")
				{
					ParseBeatmap(filepath, song);
				}
			}

			// Make sure all required attributes are loaded
			if (!song.IsValid()) 
			{
				throw new InvalidSongException("Missing required field" );
			}

			// Make sure the audio file exists
			String audioFilePath = Path.Combine(directoryPath, song.AudioFilename);
			if (!File.Exists(audioFilePath))
			{
				throw new InvalidSongException("Audio file missing." );
			}

			// Get date and time the song was downloaded
			try { song.DateDownloaded = File.GetLastWriteTime(audioFilePath); }
			catch (Exception e) { throw new InvalidSongException("Failed to load download date: ", e); }

			// Use the loaded metadata to generate some extra data that the gui needs
			song.GenerateGUIData();

			return song;
		}

		/**
		 * Returns the song id from the directory name. If the song is invalid, -1 is returned instead.
		 */
		public static int GetSongID(String directoryName)
		{
			String[] split = directoryName.Split(' ');
			
			int id = int.TryParse(split[0], out id) ? id : -1;

			return id;
		}

		
		/**
		 * Parses a beatmap in the .osu file format and adds it to the SongData object. Parsing is normally 
		 * done multiple times and any duplicate data will simply get overwritten.
		 * 
		 * <exception cref="InvalidSongException"></exception>
		 */
		public static void ParseBeatmap(String filepath, SongData song)
		{
			// The current section that has been entered. If the section is irrelevant, it's skipped instead.
			String currentSection = null;

			foreach (String line in File.ReadLines(filepath))
			{
				
				// If the line has brackets, decode it as a section tag, otherwise, read data from it
				if (line.StartsWith("["))
				{
					// If the line is invalid, skip it
					if (line.IndexOf(']') == -1) continue;

					currentSection = null;

					// If the new section is relevant, enter it.
					String newSection = line.Substring(1, line.IndexOf(']')-1);
					foreach (String section in RelevantSections)
					{
						if (newSection == section)
						{
							currentSection = section;
							break;
						}
					}

					// If all relevant data has been read, stop parsing
					//if (newSection == endSection) break;
				}
				else
				{
					// Wait for a relevant section
					if (currentSection == null) continue;
				
					// Split the line into key and value
					String[] split = line.Split(new char[]{':'}, 2);

					// If the line is invalid, skip it
					if (split.Length < 2) continue;

					String key = split[0].Trim();
					String value = split[1].Trim();

					// Read the data
					song.ReadData(key, value, currentSection);
				}
			}
		}

		// Sections of the .osu file with relevant data
		private static readonly String[] RelevantSections = { "General", "Metadata", "Difficulty", "HitObjects" };

		// Once this section has been reached, there is nothing interesting left to read in the file
		//private static readonly String endSection = "Events";

	}

	public class InvalidSongException : Exception
	{
		public InvalidSongException() : base() {}

		public InvalidSongException(String message) : base(message) { }

		public InvalidSongException(String message, Exception innerException) : base(message, innerException) { }
	}
}
