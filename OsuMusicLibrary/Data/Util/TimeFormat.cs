﻿using System;

namespace OsuMusicLibrary.Data.Util
{
	public class TimeFormat
	{
		private readonly DateTime then;

		public TimeFormat(DateTime then)
		{
			this.then = then;
		}

		public override string ToString()
		{
			return TimeSince(DateTime.Now, then);
		}

		public static String TimeSince(DateTime now, DateTime then)
		{
			if (now.Year == then.Year)
			{
				if (now.Month == then.Month)
				{
					if (now.Day == then.Day)
					{
						if (now.Hour == then.Hour)
						{
							if (now.Minute >= then.Minute)
							{
								return MinutesAgo(now.Minute - then.Minute);
							}
						}
						else if (now.Hour > then.Hour)
						{
							return HoursAgo(now.Hour - then.Hour, now.Minute - then.Minute);
						}
					}
					else if (now.Day > then.Day)
					{
						return DaysAgo(now.Day - then.Day, now.Hour - then.Hour, now.Minute - then.Minute);
					}
				}
				else if (now.Month > then.Month)
				{
					return MonthsAgo(now.Month - then.Month, now.Day - then.Day, now.Hour - then.Hour, now.Minute - then.Minute, then);
				}
			}
			else if (now.Year > then.Year)
			{
				return YearsAgo(now.Year - then.Year, now.Month - then.Month, now.Day - then.Day, now.Hour - then.Hour, now.Minute - then.Minute, then);
			}
			return "Time traveler! :D";
		}

		public static String MinutesAgo(int minutes)
		{
			return MinutesToString(minutes);
		}

		public static String HoursAgo(int hours, int minutes)
		{
			if(minutes < 0)
			{
				minutes += 60;
				hours--;
			}

			return hours == 0 ? MinutesAgo(minutes) : HoursToString(hours);
		}

		public static String DaysAgo(int days, int hours, int minutes)
		{
			if (minutes < 0)
			{
				minutes += 60;
				hours--;
			}
			if (hours < 0)
			{
				hours += 24;
				days--;
			}

			return days == 0 ? HoursAgo(hours, minutes) : DaysToString(days);
		}

		public static String MonthsAgo(int months, int days, int hours, int minutes, DateTime then)
		{
			if (minutes < 0)
			{
				minutes += 60;
				hours--;
			}
			if (hours < 0)
			{
				hours += 24;
				days--;
			}
			if (days < 0)
			{
				days += DateTime.DaysInMonth(then.Year, then.Month);
				months--;
			}

			return months == 0 ? DaysAgo(days, hours, minutes) : MonthsToString(months);
		}

		public static String YearsAgo(int years, int months, int days, int hours, int minutes, DateTime then)
		{
			if (minutes < 0)
			{
				minutes += 60;
				hours--;
			}
			if (hours < 0)
			{
				hours += 24;
				days--;
			}
			if (days < 0)
			{
				days += DateTime.DaysInMonth(then.Year, then.Month); ;
				months--;
			}
			if (months < 0)
			{
				months += 12;
				years--;
			}

			return years == 0 ? MonthsAgo(months, days, hours, minutes, then) : YearsToString(years);
		}

		public static String MinutesToString(int minutes)
		{
			if (minutes == 0) return "A moment ago!";

			if (minutes == 1)
			{
				return minutes + " minute ago";
			}
			else
			{
				return minutes + " minutes ago";
			}
		}

		public static String HoursToString(int hours)
		{
			if (hours == 1)
			{
				return hours + " hour ago";
			}
			else
			{
				return hours + " hours ago";
			}
		}

		public static String DaysToString(int days)
		{
			if (days == 1)
			{
				return days + " day ago";
			}
			else
			{
				return days + " days ago";
			}
		}

		public static String MonthsToString(int months)
		{
			if (months == 1)
			{
				return months + " month ago";
			}
			else
			{
				return months + " months ago";
			}
		}

		public static String YearsToString(int years)
		{
			if (years == 1)
			{
				return years + " year ago";
			}
			else
			{
				return years + " years ago";
			}
		}
	}
}
