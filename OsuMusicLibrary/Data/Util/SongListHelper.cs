﻿using System.Collections.Generic;
using System.Linq;
using OsuMusicLibrary.Data.Types;

namespace OsuMusicLibrary.Data.Util
{
	class SongListHelper
	{
		/** 
		 * Create a list of songs from the list of song ids
		 */
		public static List<SongData> SongsFromIDs(IEnumerable<int> ids)
		{
			return (from songID in ids where GlobalData.SongCache.Songs.ContainsKey(songID) select GlobalData.SongCache.Songs[songID]).ToList();
		}

		/**
		 * Generates a new list with all the songs that failed the filter removed.
		 */
		public static List<SongData> FilterList(IEnumerable<SongData> list, Filter filter)
		{
			// Add all the available songs to a list
			return list.Where(song => filter.IsVisible(song)).ToList();
		}

		/**
		 * Sorts the list based on a given sort directive.
		 */
		public static List<SongData> SortList(IEnumerable<SongData> list, SortDirective sort)
		{
			// Sort the list of songs, either ascending or descending
			List<SongData> sortedList;
			if (sort.IsAscending)
			{
				sortedList = list.OrderBy(sort.SortProperty).ThenBy(song => song.SongID).ToList();
			}
			else
			{
				sortedList = list.OrderByDescending(sort.SortProperty).ThenByDescending(song => song.SongID).ToList();
			}
			return sortedList;
		}
	}
}
