﻿using System;
using System.IO;
using System.Linq;

namespace OsuMusicLibrary.Data.Util
{
	class InstallLocator
	{
		/**
		 * Attempts to find the osu! installation directory in standard locations. Either teturns a valid 
		 * installation path, or null if none could be detected.
		 */
		public static String TryFindInstallPath()
		{
			// Get an array of paths to test
			String[] paths = GetExpectedInstallPaths();

			// For each of the paths, see if it exists. If it does, return it.
			return paths.FirstOrDefault(IsValidInstallPath);
			// If no installation path was detected, return null
			
		}

		/**
		 * Determines whether a directory is a valid osu! installation directory.
		 */
		public static bool IsValidInstallPath(String path)
		{
			// If the installation path exists and it contains a "songs" directory, it's considered valid.
			return Directory.Exists(path) && Directory.Exists(Path.Combine(path, "songs"));
		}

		/**
		 * Returns typical installation paths for osu!
		 */
		private static String[] GetExpectedInstallPaths()
		{
			return new String[] {"C:/Program Files (x86)/osu!/", "C:/Program Files/osu!/"};
		}
	}
}
