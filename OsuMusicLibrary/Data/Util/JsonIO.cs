﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace OsuMusicLibrary.Data.Util
{
	class JsonIO
	{
		/**
		 * <exception cref="System.ArgumentException">File path is null, empty or contains illegal characters</exception>
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 * <exception cref="JsonReaderException">Json deserialization failed</exception>
		 */
		public static T LoadObject<T>(String filepath)
		{
			String jsonData = File.ReadAllText(filepath);
			var loadedObject = JsonConvert.DeserializeObject<T>(jsonData);
			if (loadedObject == null) throw new JsonReaderException();
			return loadedObject;
		}


		/**
		 * <exception cref="System.ArgumentException">File path is null, empty or contains illegal characters</exception>
		 * <exception cref="System.IO.IOException">Path too long, directory not found or I/O error occured</exception>
		 * <exception cref="System.UnauthorizedAccessException">Access denied</exception>
		 */
		public static void SaveObject(String filepath, Object objectToSave, Formatting formatting = Formatting.None)
		{
			String jsonData = JsonConvert.SerializeObject(objectToSave, formatting);
			File.WriteAllText(filepath, jsonData);
		}
			
	}
}
