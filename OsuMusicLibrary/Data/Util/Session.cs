﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Types;
using OsuMusicLibrary.GUI;

namespace OsuMusicLibrary.Data.Util
{
    public class Session
    {
        public Session()
        {
            CurrentCategory = null;
            AllSongsFilter = new Filter(null);
            AllSongsSort = new SortDirective();
        }

        /** Temporary queue of songs */
        public readonly List<SongData> Queue = new List<SongData>();

        /** The currently selected category or null if "All Songs" is selected */
		private Category currentCategory;
		public Category CurrentCategory 
		{
			get { return currentCategory; }
			set 
			{ 
				currentCategory = value;
				if (!App.Initialized) return;

				MainWindow.OMLWindow.SongList.AllowDrop = currentCategory != null;

				// Find the "Remove from ..." menu item and adapt it to the new environment
				foreach (var item in MainWindow.OMLWindow.SongList.ContextMenu.Items)
				{
					MenuItem menuItem = item as MenuItem;
					if (menuItem != null && menuItem.Name == "SongListContextMenuRemoveSong")
					{
						menuItem.Visibility = currentCategory != null ? Visibility.Visible : Visibility.Collapsed;
						menuItem.Header = currentCategory != null ? "Remove From \"" + currentCategory.Name + "\"": "";
					}
					Separator separator = item as Separator;
					if (separator != null && separator.Name == "DeleteSeparator")
					{
						separator.Visibility = currentCategory != null ? Visibility.Visible : Visibility.Collapsed;
					}
				}
			} 
		}

        /** The filter belonging to the current category (or all songs list if no category is selected) */
        public Filter CurrentFilter { get { return CurrentCategory != null ? CurrentCategory.Filter : AllSongsFilter; } }

        /** The sort directive belonging to the current category (or all songs list if no category is selected) */
        public SortDirective CurrentSortDirective { get { return CurrentCategory != null ? CurrentCategory.Sort : AllSongsSort; } }

        /** Whether categories should be ignored */
        public bool AllSongsVisible { get { return CurrentCategory == null; } }


        /** The filter for the 'All Songs' list */
        public Filter AllSongsFilter { get; private set; }

        /** Getter for the library */
        public SortDirective AllSongsSort { get; private set; }


        public void ShowAllSongs()
        {
            CurrentCategory = null;
        }

		/**
		 * Try to add a song to the queue, but it if the song already exists
		 */
		public void TryAddToQueue(SongData song, int index = -1)
		{
			if (Queue.Contains(song)) return;
			if (index > 0)
			{
				Queue.Insert(index, song);
			}
			else
			{
				Queue.Add(song);
			}
		}
    }
}
