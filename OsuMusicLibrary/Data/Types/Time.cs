﻿using System;
using Newtonsoft.Json;

namespace OsuMusicLibrary.Data.Containers
{
	[JsonObject(MemberSerialization.OptIn)]
	public class Time
	{
		/** <summary> The total number of milliseconds. </summary> */
		[JsonProperty]
		public int TotalMilliSeconds { get; set; }

		public Time(int milliseconds)
		{
			TotalMilliSeconds = milliseconds;
		}

		public override string ToString()
		{
			return new TimeSpan(0,0,0,0,TotalMilliSeconds).ToString(@"mm\:ss");
		}
	}
}
