﻿namespace OsuMusicLibrary.Data.Types
{
    public enum SongContext
    {
        ALL_SONGS,
        CATEGORY,
        QUEUE
    }
}
