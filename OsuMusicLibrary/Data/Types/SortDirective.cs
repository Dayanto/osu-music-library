﻿using System;

namespace OsuMusicLibrary.Data.Types
{
    public class SortDirective
    {
        public String PropertyName { get; set; }
        public Func<SongData, object> SortProperty { get; set; }
        public bool IsAscending { get; set; }

        public SortDirective()
        {
            PropertyName = "SongID";
            SortProperty = (song => song.SongID);
            IsAscending = true;
        }
    }
}
