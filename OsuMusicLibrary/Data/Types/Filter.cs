﻿using System;
using System.Linq;
using OsuMusicLibrary.Data.Containers;

namespace OsuMusicLibrary.Data.Types
{
    //[JsonObject(MemberSerialization.OptIn)]
	public class Filter
	{
		/** The minimum rating that is displayed in the list */
		public int MinimumRating { get; set; }

		/** Whether only unrated songs should be visible. If you rate regularly, this is a list of all the new songs. */
		public bool ShowUnrated { get; set; }

		/** How recently songs need to have been downloaded to be visible */
		public Downloaded TimeSinceRequired { get; set; }

        /** The current search text */
        public String SearchText { get; set; }

		/** The current search terms split up into individual items */
		public String[] SearchTerms { get; set; }

		/** Display Easy songs */
		public bool ShowEasySongs { get; set; }

		/** Display Normal songs */
		public bool ShowNormalSongs { get; set; }

		/** Display Hard songs */
		public bool ShowHardSongs { get; set; }

		/** Display Insane songs */
		public bool ShowInsaneSongs { get; set; }

		/** The currently selected game mode or null if none */
		public GameMode? SelectedGameMode { get; set; }

        private readonly Category category;

		public Filter(Category category)
		{
			MinimumRating = 1;
			ShowUnrated = true;
            SearchText = "";
			SearchTerms = new String[0];
			
			ShowEasySongs = true;
			ShowNormalSongs = true;
			ShowHardSongs = true;
			ShowInsaneSongs = true;

			TimeSinceRequired = Downloaded.ANY_TIME;

            this.category = category;
		}

		/**
		 * Returns whether this song should be visible in the list
		 */
		public bool IsVisible(SongData song)
		{
            if (song == null) return false;

			// If a category is selected, skip songs that aren't in that category
			if (category != null)
				if (!category.SongIDs.Contains(song.SongID)) return false;

			// Filter based on ratings
			if (GlobalData.Library.Ratings.ContainsKey(song.SongID))
			{
				// Hide rated songs if the rating is too bad
				if (GlobalData.Library.Ratings[song.SongID] < MinimumRating) return false;
			}
			else
			{
				// Hide all unrated songs if they're being hidden
				if (!ShowUnrated) return false;
			}

			// Filter game mode (if one is selected)
			if (SelectedGameMode != null)
			{
				if (SelectedGameMode == GameMode.OSU			&& !song.GameModesAvailable.Contains(GameMode.OSU))				return false;
				if (SelectedGameMode == GameMode.TAIKO			&& !song.GameModesAvailable.Contains(GameMode.TAIKO))			return false;
				if (SelectedGameMode == GameMode.CATCH_THE_BEAT && !song.GameModesAvailable.Contains(GameMode.CATCH_THE_BEAT))	return false;
				if (SelectedGameMode == GameMode.OSU_MANIA		&& !song.GameModesAvailable.Contains(GameMode.OSU_MANIA))		return false;
			}

			// Filter difficulties (if any difficulty is selected)
			if (!(ShowEasySongs   && song.DifficultiesAvailable.Contains(Difficulty.EASY))   &&
				!(ShowNormalSongs && song.DifficultiesAvailable.Contains(Difficulty.NORMAL)) &&
				!(ShowHardSongs   && song.DifficultiesAvailable.Contains(Difficulty.HARD))   &&
				!(ShowInsaneSongs && song.DifficultiesAvailable.Contains(Difficulty.INSANE)))
			{
				return false;
			}

			// Filter how recent the song has to be downloaded
			if (TimeSinceRequired != Downloaded.ANY_TIME)
			{
				// Just to be sure (shouldn't be necessary)
				if (GlobalData.SongCache.Songs.ContainsKey(song.SongID))
				{
					DateTime timeDownloaded = GlobalData.SongCache.Songs[song.SongID].DateDownloaded;
					TimeSpan timeSince = DateTime.Now - timeDownloaded;
					if (TimeSinceRequired == Downloaded.TODAY		&& (timeSince.TotalDays > 1.0 || DateTime.Now.Day != timeDownloaded.Day))  return false;
					if (TimeSinceRequired == Downloaded.LAST_3_DAYS && timeSince.TotalDays > 3.0)  return false;
					if (TimeSinceRequired == Downloaded.LAST_WEEK	&& timeSince.TotalDays > 7.0)  return false;
					if (TimeSinceRequired == Downloaded.LAST_MONTH	&& timeSince.TotalDays > 30.0) return false;
				}
			}

			// Filter search results
			foreach (String searchTerm in SearchTerms)
			{
				if (!TermOccurs(searchTerm, song.SearchableData))
				{
					return false;
				}
			}

			// If it passed all the tests, it should be visible
			return true;
		}

		/**
		 * Returns whether a specific term occurs in any of the strings
		 */
		private static bool TermOccurs(String searchTerm, String[] searchableData)
		{
			foreach (String text in searchableData)
			{
				if (text.Contains(searchTerm)) return true;
			}
			return false;
		}
	}

	public enum Downloaded
	{
		TODAY,
		LAST_3_DAYS,
		LAST_WEEK,
		LAST_MONTH,
		ANY_TIME
	}
}
