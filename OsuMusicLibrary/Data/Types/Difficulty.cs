﻿using System.ComponentModel;

namespace OsuMusicLibrary.Data.Containers
{
	public enum Difficulty
	{
		[Description("Easy")]
		EASY,
		[Description("Normal")]
		NORMAL,
		[Description("Hard")]
		HARD,
		[Description("Insane")]
		INSANE 
	}

	public class DifficultyParser
	{
		public static Difficulty GetDifficulty(int overallDifficulty)
		{
			if		(overallDifficulty <= 2) return Difficulty.EASY;
			else if (overallDifficulty <= 4) return Difficulty.NORMAL;
			else if (overallDifficulty <= 6) return Difficulty.HARD;
			else							 return Difficulty.INSANE;
		}
	}
}
