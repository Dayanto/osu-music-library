﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Util;

namespace OsuMusicLibrary.Data.Types
{
    [JsonObject(MemberSerialization.OptIn)]
	public class SongData
	{
		/**
		 * Default constructs an incomplete song object. Before using the song, all required fields must be loaded. 
		 * If any of the required fields are missing, the song is discarded.
		 */
		public SongData()
		{
			SongID = -1;					//Required
			Directory = null;				//Required
			Title = "";
			TitleUnicode = "";
			Artist = "";
			ArtistUnicode = "";
			Creator = "";
			Source = "";
			Tags = new String[0];
			AudioFilename = null;			//Required
			PreviewTime = new Time(0);
			Duration = new Time(0);
			DateDownloaded = new DateTime();
		}

		/**
		 * For a song to be valid, it needs to include a number of attributes. If any of these are missing (in case the
		 * save file got damaged), the song is not considered valid and should not be used.
		 */
		public bool IsValid()
		{
			return SongID > 0 && Directory != null && AudioFilename != null;
		}

		#region Song Metadata

		/** (Required Attribute) The unique identifier that every public osu! beatmap set has */
        [JsonProperty]
		public int SongID { get; set; }

		/** (Required Attribute) The directory this Song is located in */
        [JsonProperty]
        public String Directory { get; set; }


		/** (Required Attribute) The name of the audio file for this song. (Only one per beatmap set.) */
        [JsonProperty]
        public String AudioFilename { get; set; } // General : AudioFilename

		/** The time offset in milliseconds where the song starts when playing a preview */
        [JsonProperty]
        public Time PreviewTime { get; set; } // General : PreviewTime

		/** The duration of the song in milliseconds */
        [JsonProperty]
        public Time Duration { get; set; } // General : PreviewTime

		/** The name of the song in ASCII characters */
        [JsonProperty]
        public String Title { get; set; } // Metadata : Title

		/** The name of the song using unicode characters */
        [JsonProperty]
        public String TitleUnicode { get; set; } // Metadata : TitleUnicode

		/** The name of the artist in ASCII characters. */
        [JsonProperty]
        public String Artist { get; set; } // Metadata : Artist

		/** The name of the arist using unicode characters*/
        [JsonProperty]
        public String ArtistUnicode { get; set; } // Metadata : ArtistUnicode

		/** The name of the person who made the beatmap */
        [JsonProperty]
        public String Creator { get; set; } // Metadata : Creator

		/** The Source the song came from (if it's from an anime, the source is the name of the anime) */
        [JsonProperty]
        public String Source { get; set; } // Metadata : Source

		/** The searchable tags assigned to this song */
        [JsonProperty]
        public String[] Tags { get; set; } // Metadata : Tags

		/** Available game modes for this song */
        [JsonProperty]
        public List<BeatmapData> Beatmaps = new List<BeatmapData>(); // General : Mode & Metadata : Version


		/** The date and time when the beatmap was downloaded. */
        [JsonProperty]
        public DateTime DateDownloaded { get; set; }

		/** Returns the filepath of the audiofile */
		//[JsonIgnore]
		public String AudioFilePath { get { return Path.Combine(GlobalData.Settings.SongsDirectory, Directory, AudioFilename); } }

        /** Returns the path of the directory */
        //[JsonIgnore]
        public String DirectoryPath { get { return Path.Combine(GlobalData.Settings.SongsDirectory, Directory); } }

        /** The rating that this song has */
        public int Rating { get { return GlobalData.Library.Ratings.ContainsKey(SongID) ? GlobalData.Library.Ratings[SongID] : 0; } }


		#endregion

		#region Display Properties		

		/** The version of the title that's displayed in the application */
		//[JsonIgnore]
		public String DisplayTitle { get { return GlobalData.Settings.PreferUnicode ? (TitleUnicode != "" ? TitleUnicode : Title) : (Title != "" ? Title : TitleUnicode); } }

		/** The version of the title that's displayed in the application */
		//[JsonIgnore]
		public String DisplayArtist { get { return GlobalData.Settings.PreferUnicode ? (ArtistUnicode != "" ? ArtistUnicode : Artist) : (Artist != "" ? Artist : ArtistUnicode); } }

		/** The name of the creator as it's displayed in the application */
		//[JsonIgnore]
		public String DisplayCreator { get { return Creator; } }

		/** The Source as it's displayed in the application */
		//[JsonIgnore]
		public String DisplaySource { get { return Source; } }

		/** The duration of the song in text form */
        //[JsonIgnore]
        public String DisplayTime { get { return Duration.ToString(); } }

		/** A simplified list of difficulties for this song as it's displayed in the application */
		[JsonProperty]
        public String DisplayDifficulties { get; set; }

		/** A list of the different game modes available for this song */
		[JsonProperty]
        public String DisplayGameModes { get; set; }

		/** The date the song was downloaded, displayed as the time that has past since */
        //[JsonIgnore]
        public String DisplayDateDownloaded { get { return TimeFormat.TimeSince(DateTime.Now, DateDownloaded); } }

		/** Lists all the categories this song has been added to */
		//[JsonIgnore]
        public String DisplayCategories
		{
			get
			{
				String categories = String.Empty;
				var songCategories = GlobalData.Library.GetCategoriesOfSong(this);
				foreach (Category category in songCategories)
				{
					categories += category.Name;
					if (category != songCategories.Last()) categories += ", ";
				}
				return categories;
			}
		}

		/** All the tags stitched together */
		//[JsonIgnore]
        public String DisplayTags 
		{ 
			get 
			{
				String allTags = String.Empty;
				foreach (String tag in Tags)
				{
					allTags += tag;
					if (tag != Tags.Last()) allTags += ", ";
				}
				return allTags;
			} 
		}

		/** A short summary of this song that's used in the queue */
        //[JsonIgnore]
		public String DisplaySummary { get { return DisplayTitle + " - " + DisplayArtist; } }

		#endregion

		#region Star Image Getters

		/** The image of star no. 1 */
		//[JsonIgnore]
        public String Star1Image { get { return "/OsuMusicLibrary;component/Resources/" + (GlobalData.Library.Ratings.ContainsKey(SongID) ? (GlobalData.Library.Ratings[SongID] >= 1 ? "StarSmall.png" : "StarSmallGrey.png") : ("StarSmallHalf.png")); } }
		/** The image of star no. 2 */
        //[JsonIgnore]
        public String Star2Image { get { return "/OsuMusicLibrary;component/Resources/" + (GlobalData.Library.Ratings.ContainsKey(SongID) ? (GlobalData.Library.Ratings[SongID] >= 2 ? "StarSmall.png" : "StarSmallGrey.png") : ("StarSmallHalf.png")); } }
		/** The image of star no. 3 */
        //[JsonIgnore]
        public String Star3Image { get { return "/OsuMusicLibrary;component/Resources/" + (GlobalData.Library.Ratings.ContainsKey(SongID) ? (GlobalData.Library.Ratings[SongID] >= 3 ? "StarSmall.png" : "StarSmallGrey.png") : ("StarSmallHalf.png")); } }
		/** The image of star no. 4 */
        //[JsonIgnore]
        public String Star4Image { get { return "/OsuMusicLibrary;component/Resources/" + (GlobalData.Library.Ratings.ContainsKey(SongID) ? (GlobalData.Library.Ratings[SongID] >= 4 ? "StarSmall.png" : "StarSmallGrey.png") : ("StarSmallHalf.png")); } }
		/** The image of star no. 5 */
        //[JsonIgnore]
        public String Star5Image { get { return "/OsuMusicLibrary;component/Resources/" + (GlobalData.Library.Ratings.ContainsKey(SongID) ? (GlobalData.Library.Ratings[SongID] >= 5 ? "StarSmall.png" : "StarSmallGrey.png") : ("StarSmallHalf.png")); } }

		#endregion

		#region Filter Data

		/** A list of unique difficulties available for this song */
		[JsonProperty]
        public Difficulty[] DifficultiesAvailable = new Difficulty[0];

		/** A list of unique game modes available for this song */
        [JsonProperty]
        public GameMode[] GameModesAvailable = new GameMode[0];

		/** A list of all the different words that can be used to search for this song */
        [JsonProperty]
        public String[] SearchableData = new String[0];

		#endregion

		#region Generate GUI Data

		/**
		 * Generates data that is used by the application for various purposes where the original 
		 * data has to be prepared beforehand.
		 */
		public void GenerateGUIData()
		{
			GenerateDifficultyData();
			GenerateGameModesData();
			GenerateSearchData();
		}

		/** 
		 * Generates a string representation of the list of difficulties that the GUI can use. Difficulties are simplified to the form of "[Easy], [Normal], [Hard], [Insane]" 
		 */
		private void GenerateDifficultyData()
		{
			List<Difficulty> difficulties = new List<Difficulty>();
			// Look through all the beatmap data for unique difficulties (Easy, Normal, Hard, Insane)
			foreach (BeatmapData beatmap in Beatmaps)
			{
				Difficulty difficulty = DifficultyParser.GetDifficulty(beatmap.OverallDifficulty);

				// Add the difficulty if it isn't in the list
				if (!difficulties.Contains(difficulty))
				{
					difficulties.Add(difficulty);
				}
			}

			// Sort the difficulties from easiest to hardest
			difficulties.Sort();

			// Assemble the string representing all the difficulties
			String text = String.Empty;
			foreach (Difficulty difficulty in difficulties)
			{
				text += EnumHelper.GetEnumDescription(difficulty);
				if (difficulty != difficulties.Last()) text += ", ";
			}

			DifficultiesAvailable = difficulties.ToArray();
			DisplayDifficulties = text;
		}

		/** 
		 * Generates a string representation that the GUI can use.
		 */
		private void GenerateGameModesData()
		{
			List<GameMode> gameModes = new List<GameMode>();

			// Add all the difficulties to the list 
			foreach (BeatmapData beatmap in Beatmaps)
			{
				if (!gameModes.Contains(beatmap.Mode))
				{
					gameModes.Add(beatmap.Mode);
				}
			}

			// Sort the difficulties so they always follow the order osu! -> Taiko -> Catch the Beat -> osu!mania
			gameModes.Sort();

			// Assemble the string representing all the difficulties
			String text = String.Empty;
			foreach (GameMode gameMode in gameModes)
			{
				text += /*"[" +*/ EnumHelper.GetEnumDescription(gameMode) /*+ "]"*/;
				if (gameMode != gameModes.Last()) text += ", ";
			}

			GameModesAvailable = gameModes.ToArray();
			DisplayGameModes = text;
		}

		/**
		 * Gathers search terms from different parts of the song object, so that they can quickly be searched without looking
		 * for everything while searching.
		 */
		private void GenerateSearchData()
		{
			List<String> searchTerms = new List<String>
			{
				// Gather strings from various sources
				Title,
				TitleUnicode,
				Artist,
				ArtistUnicode,
				Source,

				// Also add text from the different sources without spaces
				Title.Replace(" ", String.Empty),
				TitleUnicode.Replace(" ", String.Empty),
				Artist.Replace(" ", String.Empty),
				ArtistUnicode.Replace(" ", String.Empty),
				Source.Replace(" ", String.Empty)
			};

			// Add specified tags
			searchTerms.AddRange(Tags);

			// Filter out empty strings
			searchTerms.RemoveAll(x => x == "");

			// Convert everything to lower case
			searchTerms = searchTerms.ConvertAll(s => s.ToLower());

			SearchableData = searchTerms.ToArray();
		}

		#endregion

		#region Read Data

		/**
		 * Recieves data when the different beatmap files for this object are loaded. Most of the new data simply overwrites 
		 * previous data if it's duplicated across multiple files, but game modes and difficulties do not. 
		 */
		public void ReadData(String key, String value, String section)
		{
			// The 'HitObjects' section is different from the other sections in that it doesn't use a key, so it's loaded in its own way
			if (section == "HitObjects")
			{
				String[] split = key.Split(',');
				int parsedMilliSeconds;
				if (int.TryParse(split[2], out parsedMilliSeconds))
				{
					// Only change the time if the current note comes later than any previous, so that the last note in any of the beatmap determines the time.
					Duration.TotalMilliSeconds = (parsedMilliSeconds > Duration.TotalMilliSeconds) ? parsedMilliSeconds : Duration.TotalMilliSeconds;
				}
				return;
			}

			// Parse the value in case it's an int
			int parsedValue;
			bool validInt = int.TryParse(value, out parsedValue);

			// Store data
			switch (key)
			{
				//########### [General] ############

				case "AudioFilename":
				{
					AudioFilename = value;
					break;
				}
				case "PreviewTime":
				{
					PreviewTime = new Time(parsedValue);
					break;
				}
				case "Mode":
				{
					if (validInt && typeof(GameMode).IsEnumDefined(parsedValue))
					{
						// Add incomplete beatmap data to the list, containing only the game mode. The difficulty is parsed later.
						Beatmaps.Add(new BeatmapData((GameMode)parsedValue));
					}
					break;
				}

				//########### [Metadata] ############

				case "Title":
				{
					Title = value;
					break;
				}
				case "TitleUnicode":
				{
					TitleUnicode = value;
					break;
				}
				case "Artist":
				{
					Artist = value;
					break;
				}
				case "ArtistUnicode":
				{
					ArtistUnicode = value;
					break;
				}
				case "Creator":
				{
					Creator = value;
					break;
				}
				case "Version":
				{
					if (Beatmaps.Count > 0 && Beatmaps.Last().VersionName == null)
					{
						// If the 'Mode' field was previously loaded, fill in the missing difficulty data for the latest beatmap object.
						Beatmaps.Last().VersionName = value;
					}
					else
					{
						// If the 'Mode' field is missing, there is no default data to replace, so a new object first has to be created with the default 'OSU' game mode.
						Beatmaps.Add(new BeatmapData(GameMode.OSU, value));
					}
					break;
				}
				case "Source":
				{
					Source = value;
					break;
				}
				case "Tags":
				{
					Tags = value.Split(new[]{" "}, StringSplitOptions.RemoveEmptyEntries);
					break;
				}

				//########### [Metadata] ############

				case "OverallDifficulty":
				{
					if (validInt && parsedValue >= 0)
					{
						if (Beatmaps.Count > 0 && Beatmaps.Last().OverallDifficulty < 0)
						{
							// If the 'OverallDifficulty' field was previously loaded, fill in the missing difficulty data for the latest beatmap object.
							Beatmaps.Last().OverallDifficulty = parsedValue;
						}
						else
						{
							// If the 'OverallDifficulty' field is missing, there is no default data to replace in, so a new object first has to be created with the default 'OSU' game mode and a blank version name.
							Beatmaps.Add(new BeatmapData(GameMode.OSU, "", parsedValue));
						}
					}
					break;
				}
			}
		}
		#endregion
	}
}

