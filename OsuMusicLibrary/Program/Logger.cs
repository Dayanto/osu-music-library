﻿using System;
using System.IO;
using System.Windows;

namespace OsuMusicLibrary.Program
{
	class Logger
	{
		public static void LogException(Exception e)
		{
			// Log the crash and display a brief error message to the user
			String filepath = "Crash_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".txt";
			String text = e + ":\n\n" + e.Message + "\n\n" + e.StackTrace;

			try
			{
				// Save the log file to disk
				File.WriteAllText(filepath, text);
			}
			catch (Exception){}

			try
			{
				String messageBoxText = "An error has occured. A crash log has been saved in the same directory as the program. \n\n" + e.GetType().ToString() + ": " + e.Message;
				String caption = "Unexpected error";
				var button = MessageBoxButton.OK;
				var icon = MessageBoxImage.Warning;
				MessageBox.Show(messageBoxText, caption, button, icon);
			}
			finally
			{
				App.Close();
			}
		}
	}
}
