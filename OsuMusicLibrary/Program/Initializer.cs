﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using Newtonsoft.Json;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Util;
using OsuMusicLibrary.GUI;
using OsuMusicLibrary.Media;

namespace OsuMusicLibrary.Program
{
	/**
	 * The initializer separates out the different steps that have to be performed before application can launch.
	 * Depending on the circumstances, the program might take many different routes through the different steps
	 * and control is often delegated away to separate GUI:s for user interaction.
	 */
	class Initializer
	{
		public static void Run()
		{
			// Disable auto-shutdown on last window close during initialization
			App.SetShutdownMode(ShutdownMode.OnExplicitShutdown);

			// Begin the initialization by loading the settings
			LoadSettings();
		}

		/**
		 * Attempts to load the settings file and generates a new one if that failed.
		 */
		public static void LoadSettings()
		{
			// If the settings file exists, open it
			if (File.Exists(GlobalData.SettingsFilePath))
			{
				try
				{
					// Try to load the settings (throws an error if it failed)
					GlobalData.LoadSettings();

					// If the osu! directory is valid, move on to the next step
					if (InstallLocator.IsValidInstallPath(GlobalData.Settings.OsuInstallPath))
					{
						LoadLibrary();
					}
					else if (GlobalData.Settings.OsuInstallPath == null)
					{
						// If a previous initialization was aborted, a valid settings file without a path might have been saved
						LocateOsu();
					}
					else
					{
						// If the current osu! directory isn't valid, ask the user to locate it again
						DialogBox.Display("osu! directory missing", "The osu! directory appears to have been moved. Please locate where it has been moved to.", LocateOsu);
					}
				}
				catch (UnauthorizedAccessException)
				{
					// If the user doesn't have access to the settings file, warn them about it and terminate.
					DialogBox.DisplayError("Access Denied", "Could not successfully load the settings file. Please move the application somewhere where you have read / write access or try giving it admin authority.", App.Close);
				}
				catch (JsonReaderException)
				{
					// If the settings file was corrupted, ignore it
					GlobalData.CreateNewSettings();
					DialogBox.Display("Failed to load settings", "The settings file appears to have been corrupted. This is not very serious, but you will need to locate the osu! installation path again.", LocateOsu);
				}
			}
			else
			{
				// If no settings file exists, create a new one
				GlobalData.CreateNewSettings();
				LocateOsu();
			}
		}

		/**
		 * If the settings file is missing or has an invalid path to the osu! directory, the user will have to locate it.
		 */
		public static void LocateOsu()
		{
			// Look for the osu! directory in typical locations
			String installPath = InstallLocator.TryFindInstallPath();

			if (installPath != null)
			{
				// Successfully found the osu! directory (Ask anyways in case the user has multiple installs)
				InstallationPathDialog.Display("osu! was successfully detected. Unless you happen to have multiple installs, you can skip this step.", installPath, LoadLibrary);
			}
			else
			{
				// Could not locate osu! directory. User will have to locate it manually
				InstallationPathDialog.Display("osu! could not be detected. If you installed it in a custom location, please locate it now. If you haven't yet installed osu!, do that first and come back later. ;)", "", LoadLibrary);
			}
		}

		/**
		 * After successfully loading/creating a settings file and locating the osu! directory, attempt to load the library file
		 */
		public static void LoadLibrary()
		{
			// If the library file exists, open it
			if (File.Exists(GlobalData.LibraryFilePath))
			{
				try
				{
					// Try to load the library (this will throw an error if the file has been corrupted or if blocked by the OS)
					GlobalData.LoadLibrary();
					LoadSongCache();
				}
				catch (JsonReaderException e)
				{
					// The json file seems corrupted. Either close the application or create a new empty library base on what the user chooses.
					ConfirmationDialog.Display("Failed to load library", "Your library appears to have been corrupted. Do you want to create a new one from scratch? Think carefully. A corrupted library might be salvagable, but one that is overwritten is gone forever. This might help:\n\n" + "\"" + e.Message + "\"", CreateNewLibrary, App.Close);
				}
			}
			else
			{
				// If no library exists, make a new one
				CreateNewLibrary();
			}
		}

		/**
		 * Creates a new song cache. If there exists a previous library, doing this will overwrite and replace it.
		 * It's important to be careful.
		 */
		public static void CreateNewLibrary()
		{
			GlobalData.CreateNewLibrary();
			LoadSongCache();
		}

		/**
		 * Loads the song cache. This is not a crucial operation as the cache can be rebuilt.
		 */
		public static void LoadSongCache()
        {
            // If the library file exists, open it
            if (File.Exists(GlobalData.SongCacheFilePath))
            {
				try
				{
					// Attempt to load the song cache
					GlobalData.LoadSongCache();
				}
				catch (JsonReaderException)
				{
					// If the song cache failed to load, don't bother the user, just create a new one
					GlobalData.CreateNewSongCache();
				}
            }
            else
            {
                // Create a new song cache if none exists
                GlobalData.CreateNewSongCache();
            }

            // Always run a scan when launching the application
			BuildCacheProgressBar progress = null;
            if (GlobalData.SongCache.Songs.Count == 0)
            {
                // If this is the first scan, notify the user, in case it takes some time
				progress = BuildCacheProgressBar.Display("Building Song Cache", "Scanning your osu! songs...");
            }

			BackgroundWorker sync = new BackgroundWorker();
			sync.DoWork += (sender, e) =>
			{
				// Synchronize the song cache
				GlobalData.SongCache.Synchronize(progress);

				// Call the dispatcher on the UI thread
				App.UIDispatcher.BeginInvoke( (Action)( () =>
					{
						// Close the popup if it was open
						if (progress != null) progress.ManuallyClose();

						// Open the main window
						OpenMainWindow();
					} )
				);
			};

			// Synchronize the library and then open the main window
			sync.RunWorkerAsync();
        }

		/**
		 * Complete the initialization and open the main window
		 */
		public static void OpenMainWindow()
		{
			// Now that everything has been initialized, allow the application to close when the last windows closes again
			App.SetShutdownMode(ShutdownMode.OnLastWindowClose);

			// Open the main window of the application
			MainWindow mainWindow = new MainWindow();
			mainWindow.Show();

			// Start the song scanner
			SongScanner.SetInterval(TimeSpan.FromSeconds(60));
			if (GlobalData.Settings.AutoSynch)
			{
				SongScanner.Start();
			}

			// Set the music volume
			MusicPlayer.Player.Volume = GlobalData.Settings.Volume;

			// Add protection against 
			App.Initialized = true;
		}
	}
}
