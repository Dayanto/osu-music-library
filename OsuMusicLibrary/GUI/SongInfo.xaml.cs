﻿using System.Windows;
using OsuMusicLibrary.Data.Types;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for SongInfo.xaml
	/// </summary>
	public partial class SongInfo : Window
	{
		public SongInfo(SongData song)
		{
			InitializeComponent();
			BeatmapList.ItemsSource = song.Beatmaps;
			SongTitle.Content = song.DisplayTitle;
			Artist.Content = song.DisplayArtist;
			Source.Content = song.DisplaySource;
            Time.Content = song.DisplayTime;
			Creator.Content = song.DisplayCreator;
            ID.Content = song.SongID;
			Categories.Content = song.DisplayCategories;
			Tags.Text = song.DisplayTags;
		}
	}
}
