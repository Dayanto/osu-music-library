﻿using System;
using System.Windows;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for dialogBox.xaml
	/// </summary>
	public partial class DialogBox : Window
	{
		public delegate void NextCall();
		private readonly NextCall next;

		public DialogBox(String title, String message, NextCall next)
		{
			this.next = next;
			InitializeComponent();
			if (title != null) Title = title;
			TextBlock.Text = message;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			next();
			this.Close();
		}

		public static DialogBox Display(String title, String message, NextCall next)
		{
			var box = new DialogBox(title, message, next);
			box.Show();
			return box;
		}

		public static DialogBox Display(String title, String message)
		{
			return Display(title, message, () => { /* no-op */ });
		}

		public static DialogBox DisplayError(String title, String message, NextCall next)
		{
			App.SetShutdownMode(ShutdownMode.OnLastWindowClose);
			return Display("Error: " + title, message, next);
		}
	}
}
