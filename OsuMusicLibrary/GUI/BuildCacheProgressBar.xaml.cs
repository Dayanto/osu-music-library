﻿using System;
using System.ComponentModel;
using System.Windows;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for dialogBox.xaml
	/// </summary>
	public partial class BuildCacheProgressBar : Window
	{
		public int TotalSongs { set { ProgressBar.Maximum = value; } }
		public int CurrentSong { get { return (int)ProgressBar.Value; } set { ProgressBar.Value = value; } }
		public String CurrentFolderName { set { FolderName.Text = value; } }

		private bool closable;

		public BuildCacheProgressBar(String title, String message)
		{
			InitializeComponent();
			if (title != null) Title = title;
			Message.Text = message;
			closable = false;
		}

		private void OnClosing(object sender, CancelEventArgs e)
		{
			// Cancels the close request if the window isn't closable
			e.Cancel = !closable;
		}

		public void ManuallyClose()
		{
			closable = true;
			this.Close();
		}

		public static BuildCacheProgressBar Display(String title, String message)
		{
			var progress = new BuildCacheProgressBar(title, message);
			progress.Show();
			return progress;
		}
	}
}
