﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.Data.Containers;
using OsuMusicLibrary.Data.Types;
using OsuMusicLibrary.Data.Util;
using OsuMusicLibrary.Media;
using Wpf.Util;

using WinForms = System.Windows.Forms;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window, INotifyPropertyChanged
	{
		public static MainWindow OMLWindow = null;

		private bool sliderDragged = false;

		#region Window

			#region Construction

		public MainWindow()
		{
			// Initialize the xaml stuff
			InitializeComponent();

			// Attach the list of songs to the listview, it will filter itself based on the ListViewFilter function below
			CollectionViewSource listViewSource = this.Resources["ListViewSource"] as CollectionViewSource;
			listViewSource.Source = GlobalData.ObservableSongList;

			// Attach the list of songs to the listview, it will use the queue as a filter based on QueueFilter below
			CollectionViewSource queueSource = this.Resources["QueueSource"] as CollectionViewSource;
			queueSource.Source = GlobalData.Session.Queue;

			// Add the items to the category list
			List<object> categoryItems = new List<object>();
			categoryItems.Add(new AllSongs());
			categoryItems.Add(new CategoryGroup());
			CategoryTreeView.ItemsSource = categoryItems;
			
			// Select 'All Songs' in the tree view
			this.Loaded += (sender, e) => ((TreeViewItem)CategoryTreeView.ItemContainerGenerator.ContainerFromItem(CategoryTreeView.Items[0])).IsSelected = true;

			// Set up the context menu for the song list and queue
			ContextCategories.ItemsSource = GlobalData.Library.Categories;
			QueueContextCategories.ItemsSource = GlobalData.Library.Categories;

            // Initialize the filters and sort directives
            RestoreFilteringAndSorting(GlobalData.Session.CurrentFilter, GlobalData.Session.CurrentSortDirective);

            this.PreviewKeyDown += OnKeyDownHandler;
            this.PreviewKeyUp += OnKeyUpHandler;

			// Make the window openly accessible
			OMLWindow = this;
		}

			#endregion

			#region Key Handlers

		private void OnKeyDownHandler(object sender, KeyEventArgs e)
		{
			// Modifier keys
			if (e.Key == Key.LeftShift || e.Key == Key.LeftAlt || e.Key == Key.LeftCtrl || e.Key == Key.RightShift || e.Key == Key.RightAlt || e.Key == Key.RightCtrl)
			{
				return;
			}

			if (e.Key == Key.Delete || e.Key == Key.Enter)
			{
				return;
			}

			if (e.Key == Key.Tab)
			{
				e.Handled = true;
				return;
			}

			if (!SearchBox.IsFocused)
			{
				SearchBox.Focus();
			}
		}

		private void OnKeyUpHandler(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				SearchBox.Text = "";
				GlobalData.Session.CurrentFilter.SearchText = "";
				GlobalData.Session.CurrentFilter.SearchTerms = new String[0];
			}

			if (e.Key == Key.Enter)
			{
				// If no song is selected, try to select one
				if (SongList.SelectedItems.Count == 0)
				{
					// Try to select the first currently visible song in the list
					HitTestResult hitTest = VisualTreeHelper.HitTest(SongList, new Point(10, 25));
					ListViewItem item = GetListViewItemFromEvent(null, hitTest.VisualHit) as ListViewItem;
					if (item != null && item.Content is SongData)
					{
						SongList.SelectedItem = item.Content;
					}
				}

				SongData song = SongList.SelectedItem as SongData;
				SongContext context = GlobalData.Session.CurrentCategory == null ? SongContext.ALL_SONGS : SongContext.CATEGORY;
				PlaySong(context, song);
			}

			if (e.Key == Key.Tab)
			{
				// Get the index of the current song in the list
				int itemIndex = SongList.Items.IndexOf(SongPicker.CurrentSong);
				if (itemIndex == -1) return;

				SongList.SelectedItem = SongPicker.CurrentSong;

				VirtualizingStackPanel vsp = (VirtualizingStackPanel)typeof(ItemsControl).InvokeMember("_itemsHost",
												BindingFlags.Instance | BindingFlags.GetField | BindingFlags.NonPublic,
												null, SongList, null);

				double scrollHeight = vsp.ScrollOwner.ScrollableHeight + 2;

				// itemIndex_ is index of the item which we want to show in the middle of the view
				double offset = scrollHeight * itemIndex / SongList.Items.Count;

				vsp.SetVerticalOffset(offset);

				SongList.Focus();
			}

			if (e.Key == Key.Delete)
			{
				if (QueueList.IsKeyboardFocusWithin)
				{
					RemoveSelectedSongsFromQueue();
				}
				if (SongList.IsKeyboardFocusWithin)
				{
					RemoveSelectedSongsFromCurrentCategory();
				}
				if (CategoryTreeView.IsKeyboardFocusWithin)
				{
					DeleteSelectedCategory();
				}
			}
		}

		/**
         * Helper method used by the OnKeyUpHandler
         */
		private ListViewItem GetListViewItemFromEvent(object sender, object originalSource)
		{
			DependencyObject depObj = originalSource as DependencyObject;
			if (depObj != null)
			{
				// go up the visual hierarchy until we find the list view item the click came from  
				// the click might have been on the grid or column headers so we need to cater for this  
				DependencyObject current = depObj;
				while (current != null && current != SongList)
				{
					ListViewItem ListViewItem = current as ListViewItem;
					Console.WriteLine(current.GetType());
					if (ListViewItem != null)
					{
						return ListViewItem;
					}
					current = VisualTreeHelper.GetParent(current);
				}
			}

			return null;
		}

			#endregion

		#endregion

		#region Column Width Properties (Not yet implemented)

		#region INotify
		public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private GridLength rightColumnWidth = new GridLength(0, GridUnitType.Auto);
        public GridLength RightColumnWidth
        {
            get
            {
                Console.WriteLine("RightColumnWidth GET");
                // If not yet set, get the starting value from the DataModel
                if (rightColumnWidth.IsAuto)
                    rightColumnWidth = new GridLength(270, GridUnitType.Pixel);
                return rightColumnWidth;
            }
            set
            {
                Console.WriteLine("RightColumnWidth SET");
                rightColumnWidth = value;
                OnPropertyChanged("RightColumnWidth");
            }
        }

        #endregion

        #region Common Functions

			#region Music Player

		public void SetSliderPosition(double position)
		{
			if(!sliderDragged) Slider.Value = position * Slider.Maximum;
		}

		public void PlaySong(SongContext context, SongData song)
		{
			if (song == null) return;
			if (!File.Exists(song.AudioFilePath)) return;
			MusicPlayer.PlaySong(context, song);
		}

			#endregion

			#region Categories

		private static void CreateCategory(String name)
		{
			// Add the category
			GlobalData.Library.CreateCategory(name);
		}

		private static bool VerifyCategoryName(String name)
		{
			// Check if the name is valid
			if (name != null && name.Replace(" ", "").Replace("\t", "") != String.Empty)
			{
				// Check if the category exists
				if (!GlobalData.Library.CategoryExists(name))
				{
					return true;
				}
				else
				{
					DialogBox.Display("Category Exists", "A category with that name already exists!");
				}
			}
			else
			{
				DialogBox.Display("Invalid Name", "You need to give your category a name");
			}
			return false;
		}

		private void RenameSelectedCategory(String name)
		{
			Category category = CategoryTreeView.SelectedItem as Category;
			RenameCategory(category, name);
		}

		private void RenameCategory(Category category, String name)
		{
			if (category == null) return;

			GlobalData.Library.RenameCategory(category, name);
			GlobalData.SaveLibrary();

			CollectionViewSource.GetDefaultView(CategoryTreeView.ItemsSource).Refresh();
		}

		private void DeleteSelectedCategory()
		{
			Category category = CategoryTreeView.SelectedItem as Category;
			if (category == null) return;

			var result = MessageBox.Show("Are you sure that you want to remove the category \"" + category.Name + "\"?", "Delete Category", MessageBoxButton.OKCancel);
			if (result != MessageBoxResult.OK) return;

			GlobalData.Library.RemoveCategory(category.Name);
			GlobalData.SaveLibrary();

			if (category == GlobalData.Session.CurrentCategory) GlobalData.Session.ShowAllSongs();
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		private void AddSongsToCategory(IEnumerable<Object> songs, Category category)
		{
			// Ask the user for a confirmation
			if (songs.Count() > 1 && GlobalData.Settings.ConfirmMultiActions)
			{
				MessageBoxResult result = MessageBox.Show("You're about to add " + songs.Count() + " songs to \"" + category.Name + "\". \nAre you sure? \n\n(These messages can be disabled in the settings)", "Add to category", MessageBoxButton.OKCancel, MessageBoxImage.Information);
				if (result != MessageBoxResult.OK) return;
			}

			foreach (SongData song in songs)
			{
				category.AddSong(song);
			}

			GlobalData.SaveLibrary();

			// Highlight the added songs in the list if the song list is currently showing the category being modified.
			if (GlobalData.Session.CurrentCategory == category)
			{
				SongList.SelectedItems.Clear();
				foreach (SongData song in songs)
				{
					SongList.SelectedItems.Add(song);
				}
			}
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		private void RemoveSelectedSongsFromCurrentCategory()
		{
			if (SongList.SelectedItems.Count == 0) return;

			Category category = GlobalData.Session.CurrentCategory;
			IEnumerable<Object> songs = (IEnumerable<Object>)SongList.SelectedItems;

			if (category == null)
			{
				DialogBox.Display("Invalid Operation", "Cannot remove songs from 'All Songs'");
				return;
			}

			// Ask the user for a confirmation
			if (GlobalData.Settings.ConfirmDelete || (songs.Count() > 1 && GlobalData.Settings.ConfirmMultiActions))
			{
				MessageBoxResult result = MessageBox.Show("You're about to remove " + songs.Count() + " song" + (songs.Count() == 1 ? "" : "s") + " from \"" + category.Name + "\". \nAre you sure? \n\n(These messages can be disabled in the settings)", "Remove from category", MessageBoxButton.OKCancel, MessageBoxImage.Information);
				if (result != MessageBoxResult.OK) return;
			}

			foreach (SongData song in songs)
			{
				category.SongIDs.Remove(song.SongID);
			}

			GlobalData.SaveLibrary();
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		private void PlayCategory(Category category)
		{
			//MusicPlayer.SetCurrentSong();
		}

			#endregion

			#region Queue

		private void SaveQueue()
		{
			if (QueueList.Items.Count > 0)
			{
				InputDialog.Display("Save queue as category", "Category Name", "New Category", "Save", VerifyCategoryName, (name) => GlobalData.Library.CreateCategoryFromQueue(name));
			}
		}

		private void ClearQueue()
		{
			GlobalData.Session.Queue.Clear();
			CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
		}

		private void AddCategoryToQueue(Category category)
		{
			if (category == null) return;

			var result = MessageBox.Show("Are you sure that you want to add all the songs from the category \"" + category.Name + "\" to the queue?", "Add Songs To Queue", MessageBoxButton.OKCancel);
			if (result != MessageBoxResult.OK) return;

			QueueList.SelectedItems.Clear();
			foreach (int songID in category.SongIDs)
			{
				SongData song;
				if (GlobalData.SongCache.Songs.TryGetValue(songID, out song))
				{
					GlobalData.Session.TryAddToQueue(song);
					QueueList.SelectedItems.Add(song);
				}
			}
			QueueList.Focus();
			CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
		}

		private void RemoveSelectedSongsFromQueue()
		{
			if (QueueList.SelectedItems.Count == 0) return;

			// Ask the user for a confirmation
			int selectedItemsCount = QueueList.SelectedItems.Count;
			if (GlobalData.Settings.ConfirmDelete || (selectedItemsCount > 1 && GlobalData.Settings.ConfirmMultiActions))
			{
				MessageBoxResult result = MessageBox.Show("You're about to remove " + selectedItemsCount + " song" + (selectedItemsCount == 1 ? "s" : "") + " from the queue.\nAre you sure? \n\n(These messages can be disabled in the settings)", "Remove from queue", MessageBoxButton.OKCancel, MessageBoxImage.Information);
				if (result != MessageBoxResult.OK) return;
			}

			// The selected list needs to be copied to avoid issues when removing elements from the same list
			List<SongData> tempList = new List<SongData>();
			foreach (SongData song in QueueList.SelectedItems) tempList.Add(song);

			// Remove songs from the queue
			foreach (SongData song in tempList)
			{
				if (GlobalData.Session.Queue.Contains(song))
				{
					GlobalData.Session.Queue.Remove(song);
					CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
				}
			}
		}

			#endregion

			#region Filters

		private void RestoreFilteringAndSorting(Filter filter, SortDirective sort)
        {
            // Restore previous sorting
            GridViewSort.UpdateColumnSort(sort.PropertyName, sort.IsAscending);

            // Set the checkboxes to their respective states
            Easy.IsChecked		  = filter.ShowEasySongs;
            Normal.IsChecked	  = filter.ShowNormalSongs;
            Hard.IsChecked		  = filter.ShowHardSongs;
            Insane.IsChecked	  = filter.ShowInsaneSongs;
            ShowUnrated.IsChecked = filter.ShowUnrated;

            // Set the text in te text box
            SearchBox.Text = filter.SearchText;

            // Set the state of the different combo boxes
            
            // Game modes
            if (filter.SelectedGameMode == null) ComboBox_Modes.SelectedItem = AnyMode;
            else if (filter.SelectedGameMode == GameMode.OSU) ComboBox_Modes.SelectedItem = Osu;
            else if (filter.SelectedGameMode == GameMode.TAIKO) ComboBox_Modes.SelectedItem = Taiko;
            else if (filter.SelectedGameMode == GameMode.CATCH_THE_BEAT) ComboBox_Modes.SelectedItem = CatchTheBeat;
            else if (filter.SelectedGameMode == GameMode.OSU_MANIA) ComboBox_Modes.SelectedItem = OsuMania;
            
            // Minimum Rating
            if (filter.MinimumRating == 1) ComboBox_Ratings.SelectedItem = MinRating1;
            else if (filter.MinimumRating == 2) ComboBox_Ratings.SelectedItem = MinRating2;
            else if (filter.MinimumRating == 3) ComboBox_Ratings.SelectedItem = MinRating3;
            else if (filter.MinimumRating == 4) ComboBox_Ratings.SelectedItem = MinRating4;
            else if (filter.MinimumRating == 5) ComboBox_Ratings.SelectedItem = MinRating5;
            else if (filter.MinimumRating == 6) ComboBox_Ratings.SelectedItem = MinRating6;
            
            // Downloaded
            if (filter.TimeSinceRequired == Downloaded.ANY_TIME) ComboBox_Downloaded.SelectedItem = AnyTime;
            else if (filter.TimeSinceRequired == Downloaded.TODAY) ComboBox_Downloaded.SelectedItem = Today;
            else if (filter.TimeSinceRequired == Downloaded.LAST_3_DAYS) ComboBox_Downloaded.SelectedItem = Last3Days;
            else if (filter.TimeSinceRequired == Downloaded.LAST_WEEK) ComboBox_Downloaded.SelectedItem = LastWeek;
            else if (filter.TimeSinceRequired == Downloaded.LAST_MONTH) ComboBox_Downloaded.SelectedItem = LastMonth;
        }

			#endregion

			#region Files

		private static void ExportAudioFiles(IEnumerable<Object> songs)
		{
			if (songs.Count() == 0) return;

			var dialog = new WinForms.FolderBrowserDialog();
			var resultFolder = dialog.ShowDialog();

			if (resultFolder == WinForms.DialogResult.OK)
			{
				String selectedPath = dialog.SelectedPath;

				// Ask the user for confirmation
				var resultConfirm = MessageBox.Show("You're about to copy " + songs.Count() + " song files to the directory \"" + selectedPath + "\".\nAre you sure? \n\n", "Export songs", MessageBoxButton.OKCancel, MessageBoxImage.Information);
				if (resultConfirm != MessageBoxResult.OK) return;

				var resultDuplicates = MessageBox.Show("Do you want to overwrite songs with the same name? \n(If not, the song ID will be included in the name to distinguish songs)\n\n", "Overwrite duplicates", MessageBoxButton.YesNo, MessageBoxImage.Question);

				foreach (SongData song in songs)
				{
					var idAndLabel = song.Directory.Split(new[] {' '}, 2);
					if (idAndLabel.Length < 2) break;
					String filepath = Path.Combine(selectedPath, idAndLabel[1] + (resultDuplicates != MessageBoxResult.Yes ? (" (" + idAndLabel[0] + ")") : "") + Path.GetExtension(song.AudioFilePath));
					File.Copy(song.AudioFilePath, filepath, true);
				}
			}
		}

		#endregion

		#endregion

		/*########################## Event activated methods ##################################*/

		#region Menu Bar

		private void OpenSettings_Click(object sender, RoutedEventArgs e)
		{
			SettingsWindow.Display();
		}

		private void Exit_Click(object sender, RoutedEventArgs e)
		{
			App.Close();
		}

		private void RebuildCache_Click(object sender, RoutedEventArgs e)
		{
			bool musicPlaying = MusicPlayer.Playing;

			// Stop the music player
			MusicPlayer.Pause();

			// Stop the timer
			MusicPlayer.StopTimer();

			// Disable the main window
			this.IsEnabled = false;

			// Wipe the previous song cache
			GlobalData.CreateNewSongCache();

			// Show a progressbar for the song loading
			BuildCacheProgressBar progress = BuildCacheProgressBar.Display("Rebuilding Song Cache", "Scanning your osu! songs...");

			BackgroundWorker sync = new BackgroundWorker();
			sync.DoWork += (sender2, e2) =>
			{
				// Synchronize the song cache
				GlobalData.SongCache.Synchronize(progress);

				// Call the dispatcher on the UI thread
				App.UIDispatcher.BeginInvoke((Action)( () =>
					{
						// Close the popup if it was open
						if (progress != null) progress.ManuallyClose();

						// Reactivate the main window again
						this.IsEnabled = true;

						// Start the timer again
						MusicPlayer.StartTimer();

						if (musicPlaying) MusicPlayer.Play();
					} 
				));
			};

			// Synchronize the library and then reactivate the main window
			sync.RunWorkerAsync();
		}

		private void CreateCategory_Click(object sender, RoutedEventArgs e)
		{
			InputDialog.Display("Create a new category", "Category Name", "New Category", "Create", VerifyCategoryName, CreateCategory);
		}

		private void SaveQueue_Click(object sender, RoutedEventArgs e)
		{
			SaveQueue();
		}

		#endregion

		#region Music Player

		/**
		 * Responds to the play / pause button being pressed
		 */
		private void ButtonClickPlayPause(object sender, RoutedEventArgs e)
		{
			if (MusicPlayer.Player.Source == null) return;

			if (PlayPause.Content == FindResource("Play"))
			{
				if (MusicPlayer.Player.NaturalDuration != MusicPlayer.Player.Position)
				{
					MusicPlayer.Play();
				}
			}
			else
			{
				MusicPlayer.Pause();
			}
		}

		/**
		 * Responds to the next button being pressed
		 */
		private void ButtonClickNext(object sender, RoutedEventArgs e)
		{
			MusicPlayer.PlayNextSong();
		}

		/**
		 * Responds to the next button being pressed
		 */
		private void ButtonClickPrevious(object sender, RoutedEventArgs e)
		{
			MusicPlayer.PlayPreviousSong();
		}

		private void SliderPressed(object sender, MouseButtonEventArgs e)
		{
			sliderDragged = true;
		}

		private void SliderReleased(object sender, MouseButtonEventArgs e)
		{
			MusicPlayer.SetTime(Slider.Value / Slider.Maximum);
			sliderDragged = false;
		}

		#endregion

		#region ListView

			#region Filters

		private void ListViewFilter(object sender, FilterEventArgs e)
		{
			SongData song = e.Item as SongData;
			e.Accepted = GlobalData.Session.CurrentFilter.IsVisible(song);
		}

		private void QueueFilter(object sender, FilterEventArgs e)
		{
			SongData song = e.Item as SongData;
			e.Accepted = GlobalData.Session.Queue.Contains(song);
		}

			#endregion

			#region Sort

        private void SortByColumn(Func<SongData, object> sortProperty, String propertyName)
        {
            SortDirective sort = GlobalData.Session.CurrentSortDirective;

            if (sort.PropertyName != propertyName) sort.IsAscending = true;
            else sort.IsAscending = !sort.IsAscending;

            sort.SortProperty = sortProperty;
            sort.PropertyName = propertyName;

            GridViewSort.UpdateColumnSort(sort.PropertyName, sort.IsAscending);
        }

        private void SortByTitle(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DisplayTitle, "DisplayTitle");
        }
        private void SortByArtist(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DisplayArtist, "DisplayArtist");
        }
        private void SortByTime(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.Duration.TotalMilliSeconds, "Duration.TotalMilliSeconds");
        }
        private void SortBySource(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DisplaySource, "DisplaySource");
        }
        private void SortByRating(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.Rating, "Rating");
        }
        private void SortByDownloaded(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.DateDownloaded, "DateDownloaded");
        }
        private void SortByCreator(object sender, RoutedEventArgs e)
        {
            SortByColumn(song => song.Creator, "Creator");
        }

        #endregion

			#region Mouse Events

		protected void SongListDoubleClicked(object sender, MouseButtonEventArgs e)
		{
			SongData song = ((ListViewItem)sender).Content as SongData;
            SongContext context = GlobalData.Session.CurrentCategory == null ? SongContext.ALL_SONGS : SongContext.CATEGORY;
			PlaySong(context, song);
		}

		private bool clickingSongListItem = false;
		private bool draggingSongListItem = false;

		protected void SongListPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var source = e.OriginalSource as DependencyObject;

			while (source is ContentElement) source = LogicalTreeHelper.GetParent(source);
			while (source != null && !(source is ListViewItem)) source = VisualTreeHelper.GetParent(source);

			var item = source as ListViewItem;
			if (item == null) return;

			draggingSongListItem = true;

			// If the item being clicked is part of the current selection, delay altering the selection
			if (item.IsSelected && SongList.SelectedItems.Count > 1)
			{
				clickingSongListItem = true;
				e.Handled = true;
			}
		}

		protected void SongListPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (clickingSongListItem)
			{
				clickingSongListItem = false;
				ListViewItem item = sender as ListViewItem;

				item.RaiseEvent(new MouseButtonEventArgs(Mouse.PrimaryDevice, Environment.TickCount, MouseButton.Left) { RoutedEvent = Mouse.MouseDownEvent });
			}
		}

		protected void SongListMouseLeave(object sender, MouseEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed && sender is ListViewItem && draggingSongListItem)
			{
				ListViewItem draggedItem = sender as ListViewItem;

				if (!draggedItem.IsSelected) return;

				// Get a list of the selected items in the order they appear in the list
				SongData[] songs = new SongData[SongList.SelectedItems.Count];
				SongList.SelectedItems.CopyTo(songs, 0);
				List<SongData> selectedItems = SongListHelper.SortList(songs, GlobalData.Session.CurrentSortDirective);

				// Initiate drag drop
				DataObject data = new DataObject("Songs", selectedItems);
				DragDrop.DoDragDrop(draggedItem, data, DragDropEffects.Move);
			}

			clickingSongListItem = false;
			draggingSongListItem = false;
		}

		void SongListDeselect(object sender, MouseButtonEventArgs e)
		{
			SongList.SelectedItem = null;
		}

			#endregion

			#region Star Buttons

		private void ButtonClick_Rate1Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 1);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate2Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 2);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate3Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 3);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate4Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 4);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }
        private void ButtonClick_Rate5Star(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            SongData song = button.DataContext as SongData;
            GlobalData.Library.RateSong(song, 5);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }

			#endregion
			
			#region Drop Event

		/**
		 * Handle drop events for the song lists's listbox
		 */
		void SongListDropped(object sender, DragEventArgs e)
		{
			// Only drop songs from the queue
			if (e.Data.GetData("QueueDragStart") == null) return;

			Category category = GlobalData.Session.CurrentCategory;

			// If it was somehow possible to drop something on the 'All Songs' list, skip
			if (category == null) return;

			// Get the list of songs being dropped
			var droppedSongs = e.Data.GetData("Songs") as List<SongData>;
			if (droppedSongs == null) return;

			AddSongsToCategory(droppedSongs, GlobalData.Session.CurrentCategory);
		}

			#endregion

			#region Context Menu


		/**
		 * Play songs from the context menu
		 */
		private void ContextMenu_Play(object sender, RoutedEventArgs e)
		{
            SongContext context = GlobalData.Session.CurrentCategory == null ? SongContext.ALL_SONGS : SongContext.CATEGORY;
			PlaySong(context, (SongData)SongList.SelectedItem);
		}

		private void ContextMenu_MoreInfo(object sender, RoutedEventArgs e)
		{
			SongData song = (SongData)SongList.SelectedItem;
			if (song != null)
			{
				SongInfo window = new SongInfo(song);
				window.Show();
			}
		}

		private void ContextMenu_Export(object sender, RoutedEventArgs e)
		{
			ExportAudioFiles((IEnumerable<Object>)SongList.SelectedItems);
		}

        private void ContextMenu_ShowFolder(object sender, RoutedEventArgs e)
        {
            SongData song = (SongData)SongList.SelectedItem;
            if (song != null)
            {
                Process.Start(song.DirectoryPath);
            }
        }

        private void ContextMenu_AddToCategory(object sender, RoutedEventArgs e)
        {
            if (SongList.SelectedItems.Count == 0) return;

            MenuItem menuItem = sender as MenuItem;
            Category category = GlobalData.Library.GetCategory((String)menuItem.Header);

			if (category != null) AddSongsToCategory((IEnumerable<Object>)SongList.SelectedItems, category);
        }


        private void ContextMenu_AddToQueue(object sender, RoutedEventArgs e)
        {
            if (SongList.SelectedItems.Count == 0) return;

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 1 && GlobalData.Settings.ConfirmMultiActions)
            {
                var result = MessageBox.Show("You're about to add " + SongList.SelectedItems.Count + " songs to the queue.\nAre you sure? \n\n(These messages can be disabled in the settings)", "Add to queue", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            foreach (SongData song in SongList.SelectedItems)
            {
				GlobalData.Session.TryAddToQueue(song);
            }

            CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
        }

        private void ContextMenu_RemoveFromCategory(object sender, RoutedEventArgs e)
        {
			RemoveSelectedSongsFromCurrentCategory();
        }


		//  Rate songs via the context menu (1-5)

        private void RateSongsFromContextMenu(int rating)
        {
            if (SongList.SelectedItems.Count == 0) return;

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 1 && GlobalData.Settings.ConfirmMultiActions)
            {
				var result = MessageBox.Show("You're about to give " + SongList.SelectedItems.Count + " songs a " + rating + " star rating.\nAre you sure? \n\n(These messages can be disabled in the settings)", "Rate song", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            // Give each selected song a rating.
            foreach (SongData song in SongList.SelectedItems.OfType<SongData>())
            {
                GlobalData.Library.RateSong(song, rating);
            }
            
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }

		private void ContextMenu_Rate1(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(1);
		}
		private void ContextMenu_Rate2(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(2);
		}
		private void ContextMenu_Rate3(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(3);
		}
		private void ContextMenu_Rate4(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(4);
		}
		private void ContextMenu_Rate5(object sender, RoutedEventArgs e)
		{
            RateSongsFromContextMenu(5);
		}

		/**
		 * Remove the rating from a song via the Source menu
		 */
		private void ContextMenu_RemoveRating(object sender, RoutedEventArgs e)
		{
            if (SongList.SelectedItems.Count == 0) return;

            // Ask the user for a confirmation
            if (SongList.SelectedItems.Count > 1 && GlobalData.Settings.ConfirmMultiActions)
            {
				MessageBoxResult result = MessageBox.Show("You're about to remove the rating from " + SongList.SelectedItems.Count + " songs.\nAre you sure? \n\n(These messages can be disabled in the settings)", "Remove rating", MessageBoxButton.OKCancel, MessageBoxImage.Information);
                if (result != MessageBoxResult.OK) return;
            }

            // Give each selected song a rating.
            foreach (SongData song in SongList.SelectedItems.OfType<SongData>())
            {
                GlobalData.Library.RemoveRating(song);
            }

            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

			#endregion

        #endregion

        #region Queue

			#region Queue Context Menu

		/**
		 * Play songs from the queue's context menu
		 */
		private void QueueContextMenu_Play(object sender, RoutedEventArgs e)
		{
			PlaySong(SongContext.QUEUE, (SongData)QueueList.SelectedItem);
		}

		private void QueueContextMenu_MoreInfo(object sender, RoutedEventArgs e)
		{
			SongData song = (SongData)QueueList.SelectedItem;
			if (song != null)
			{
				SongInfo window = new SongInfo(song);
				window.Show();
			}
		}

		private void QueueContextMenu_MoveDown(object sender, RoutedEventArgs e)
		{
			SongData selectedSong = (SongData)QueueList.SelectedItem;
			if (selectedSong == null) return;
			if (selectedSong != GlobalData.Session.Queue.Last())
			{
				int newIndex = GlobalData.Session.Queue.IndexOf(selectedSong) + 1;
				GlobalData.Session.Queue.Remove(selectedSong);
				GlobalData.Session.Queue.Insert(newIndex, selectedSong);
				CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
			}
		}

		private void QueueContextMenu_MoveUp(object sender, RoutedEventArgs e)
		{
			SongData selectedSong = (SongData)QueueList.SelectedItem;
			if (selectedSong == null) return;
			if (selectedSong != GlobalData.Session.Queue.First())
			{
				int newIndex = GlobalData.Session.Queue.IndexOf(selectedSong) - 1;
				GlobalData.Session.Queue.Remove(selectedSong);
				GlobalData.Session.Queue.Insert(newIndex, selectedSong);
				CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
			}
		}

		private void QueueContextMenu_AddToCategory(object sender, RoutedEventArgs e)
		{
			if (QueueList.SelectedItems.Count == 0) return;

			MenuItem menuItem = sender as MenuItem;
			Category category = GlobalData.Library.GetCategory((String)menuItem.Header);

			AddSongsToCategory((IEnumerable<Object>)SongList.SelectedItems, category);
		}

		private void QueueContextMenu_Remove(object sender, RoutedEventArgs e)
		{
			RemoveSelectedSongsFromQueue();
		}

			#endregion

			#region Queue Buttons

		private void QueueButtonClear(object sender, RoutedEventArgs e)
		{
			ClearQueue();
		}

		private void QueueButtonSave(object sender, RoutedEventArgs e)
		{
			SaveQueue();
		}

			#endregion

			#region Mouse Events

		protected void QueueDoubleClicked(object sender, MouseButtonEventArgs e)
		{
			SongData song = ((ListBoxItem)sender).Content as SongData;
			PlaySong(SongContext.QUEUE, song);
			PlayPause.Content = FindResource("Pause");
		}

		private bool clickingQueueItem = false;
		private bool draggingQueueItem = false;

		void QueuePreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			var source = e.OriginalSource as DependencyObject;

			while (source is ContentElement) source = LogicalTreeHelper.GetParent(source);
			while (source != null && !(source is ListBoxItem)) source = VisualTreeHelper.GetParent(source);

			var item = source as ListBoxItem;
			if (item == null) return;

			draggingQueueItem = true;

			// If the item being clicked is part of the current selection, delay altering the selection
			if (QueueList.SelectedItems.Count > 1 && item.IsSelected)
			{
				clickingQueueItem = true;
				e.Handled = true;
			}
		}

		void QueuePreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (clickingQueueItem)
			{
				clickingQueueItem = false;
				ListBoxItem item = sender as ListBoxItem;
				item.RaiseEvent(new MouseButtonEventArgs(Mouse.PrimaryDevice, Environment.TickCount, MouseButton.Left) { RoutedEvent = Mouse.MouseDownEvent });
			}
		}

		void QueueDeselect(object sender, MouseButtonEventArgs e)
		{
			QueueList.SelectedItem = null;
		}

		void QueueMouseLeave(object sender, MouseEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed && sender is ListBoxItem && draggingQueueItem)
			{
				ListBoxItem draggedItem = sender as ListBoxItem;

				// Hash the selected items for quick 'contains' testing
				HashSet<SongData> hashedSelection = new HashSet<SongData>();
				foreach (SongData song in QueueList.SelectedItems)
				{
					hashedSelection.Add(song);
				}

				// Get a list of the selected items in the order they appear in the queue
				List<SongData> selectedItems = new List<SongData>();
				foreach (SongData song in GlobalData.Session.Queue)
				{
					if (hashedSelection.Contains(song))
					{
						selectedItems.Add(song);
					}
				}

				// Initiate drag drop
				DataObject data = new DataObject();
				data.SetData("Songs", selectedItems);
				data.SetData("QueueDragStart", draggedItem.Content);
				DragDrop.DoDragDrop(draggedItem, data, DragDropEffects.Move);
			}

			clickingQueueItem = false;
			draggingQueueItem = false;
		}

			#endregion

			#region Drop Event

		/**
		 * Handle drop events for the queue's listbox
		 */
		void QueueDropped(object sender, DragEventArgs e)
		{
			// Prevent the event from triggering again on the ListBox when something is dropped on a ListBoxItem
			e.Handled = true;

			// Get the list of songs that potentially is being dropped
			List<SongData> droppedSongs = e.Data.GetData("Songs") as List<SongData>;

			// Get the category that potentially is being dropped
			Category droppedCategory = e.Data.GetData("Category") as Category;

			if (droppedCategory != null)
			{
				AddCategoryToQueue(droppedCategory);
			}

			// If anything other than a list of songs is being dropped, don't do anything
			if (droppedSongs == null) return;

			// Ask the user for a confirmation
			if (droppedSongs.Count() > 1 && GlobalData.Settings.ConfirmMultiActions)
			{
				MessageBoxResult result = MessageBox.Show("You're about to add " + droppedSongs.Count() + " songs to the queue. \nAre you sure? \n\n(These messages can be disabled in the settings)", "Add to queue", MessageBoxButton.OKCancel, MessageBoxImage.Information);
				if (result != MessageBoxResult.OK) return;
			}

			// The index where the songs will be inserted
			int insertIndex;

			if (sender is ListBoxItem)
			{
				SongData targetSong = ((ListBoxItem)sender).DataContext as SongData;
				if (targetSong == null) return;

				// The index of the song where the drop landed
				int targetIndex = GlobalData.Session.Queue.IndexOf(targetSong);

				SongData draggedQueueSong = e.Data.GetData("QueueDragStart") as SongData;

				// If the song was dragged from the queue
				if (draggedQueueSong != null)
				{
					// The index where the drag started
					int draggedIndex = GlobalData.Session.Queue.IndexOf(draggedQueueSong);

					// Insert beyond the target in the direction of the drag. (Dragging up -> insert before, dragging down -> insert after)
					insertIndex = (draggedIndex < targetIndex) ? targetIndex + 1 : targetIndex;
				}
				else
				{
					// If the drag started outside the listbox, insert before
					insertIndex = targetIndex;
				}
			}
			else
			{
				// If the songs were dropped on an empty part of the queue, add them at the end
				insertIndex = GlobalData.Session.Queue.Count;
			}

			// Whether songs are being inserted from the outside or reordered
			bool inserted = (e.Data.GetData("QueueDragStart") as SongData) == null;
			bool reordered = !inserted;

			// Deselect the current selection
			QueueList.SelectedItems.Clear();

			// Drop the songs in the list
			foreach (SongData song in droppedSongs)
			{
				// Get the index of the song
				int songIndex = GlobalData.Session.Queue.IndexOf(song);
				
				// Only remove songs if they're being reordered
				if (reordered)
				{
					// When reordering songs, begin by removing it and updating the target index
					if (songIndex != -1)
					{
						GlobalData.Session.Queue.Remove(song);
						if (songIndex < insertIndex) insertIndex--;
					}
				}

				// Don't insert the song if it already exists unless it's being reordered
				if (reordered || (inserted && songIndex == -1))
				{
					// Insert the song at the target index
					GlobalData.Session.Queue.Insert(insertIndex, song);

					// Move the target, so that the next item is inserted after current one
					insertIndex++;
				}

				// Highlight the song
				QueueList.SelectedItems.Add(song);
			}


			// Give the queue focus
			QueueList.Focus();

			CollectionViewSource.GetDefaultView(QueueList.ItemsSource).Refresh();
		}

		#endregion

		#endregion

		#region Filter Updates

			#region Difficulties

		/**
		 * Show all songs containing the difficulty easy
		 */
		private void CheckBox_Easy(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowEasySongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Show all songs containing the difficulty normal
		 */
		private void CheckBox_Normal(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowNormalSongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Show all songs containing the difficulty hard
		 */
		private void CheckBox_Hard(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowHardSongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

		/**
		 * Show all songs containing the difficulty insane
		 */
		private void CheckBox_Insane(object sender, RoutedEventArgs e)
		{
			CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowInsaneSongs = box.IsChecked.Value;
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

			#endregion

			#region Modes

		/**
		 * Update the currently visible modes based on the new combo box selection
		 */
		private void ComboBox_Modes_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
            if (ComboBox_Modes.SelectedItem == AnyMode)      GlobalData.Session.CurrentFilter.SelectedGameMode = null;
            if (ComboBox_Modes.SelectedItem == Osu)          GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.OSU;
            if (ComboBox_Modes.SelectedItem == Taiko)        GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.TAIKO;
            if (ComboBox_Modes.SelectedItem == CatchTheBeat) GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.CATCH_THE_BEAT;
            if (ComboBox_Modes.SelectedItem == OsuMania)     GlobalData.Session.CurrentFilter.SelectedGameMode = GameMode.OSU_MANIA;

			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

			#endregion

			#region Ratings

		/**
		 * Update the currently visible ratings based on the new combo box selection
		 */
		private void ComboBox_Ratings_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if      (ComboBox_Ratings.SelectedItem == MinRating1) GlobalData.Session.CurrentFilter.MinimumRating = 1;
			else if (ComboBox_Ratings.SelectedItem == MinRating2) GlobalData.Session.CurrentFilter.MinimumRating = 2;
			else if (ComboBox_Ratings.SelectedItem == MinRating3) GlobalData.Session.CurrentFilter.MinimumRating = 3;
			else if (ComboBox_Ratings.SelectedItem == MinRating4) GlobalData.Session.CurrentFilter.MinimumRating = 4;
			else if (ComboBox_Ratings.SelectedItem == MinRating5) GlobalData.Session.CurrentFilter.MinimumRating = 5;
            else if (ComboBox_Ratings.SelectedItem == MinRating6) GlobalData.Session.CurrentFilter.MinimumRating = 6;

			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

        /**
		 * Update whether unrated songs should be displayed
		 */
        private void CheckBox_ShowUnrated(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            GlobalData.Session.CurrentFilter.ShowUnrated = box.IsChecked.Value;
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
        }

			#endregion

			#region Downloaded

		/**
		 * Update how recently downloaded songs are shown based on the new combo box selection
		 */
		private void ComboBox_Downloaded_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (ComboBox_Downloaded.SelectedItem == AnyTime) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.ANY_TIME;
			if (ComboBox_Downloaded.SelectedItem == Today) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.TODAY;
			if (ComboBox_Downloaded.SelectedItem == Last3Days) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.LAST_3_DAYS;
			if (ComboBox_Downloaded.SelectedItem == LastWeek) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.LAST_WEEK;
			if (ComboBox_Downloaded.SelectedItem == LastMonth) GlobalData.Session.CurrentFilter.TimeSinceRequired = Downloaded.LAST_MONTH;

			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

			#endregion

			#region Search

		/**
		 * Updates the search whenever something is typed in the search box
		 */
		private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			String input = SearchBox.Text.ToLower();
			List<String> searchTerms = new List<String>();

			// Skip splitting by spaces inside of quotes
			bool insideQuotes = false;
			foreach (String chunk in input.Split('\"'))
			{
				// If the current chunk is inside quotes, add it to the list of search terms in one piece
				if (insideQuotes) searchTerms.Add(chunk.Trim());

				// If outside of quotes, split up every word into a separate string
				if (!insideQuotes) searchTerms.AddRange(chunk.Split(new[]{" "}, StringSplitOptions.RemoveEmptyEntries));

				// For every quotation mark, flip whether the text following it is inside or outside quotes
				insideQuotes = !insideQuotes;
			}

			// Filter out any potential spaces from the results (only matters for text inside quotes)
			searchTerms.RemoveAll(x => x == "");

            // Store the current input text
            GlobalData.Session.CurrentFilter.SearchText = SearchBox.Text;

			// Update the current search terms and refresh the filtering
			GlobalData.Session.CurrentFilter.SearchTerms = searchTerms.ToArray();
			CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();
		}

			#endregion

		#endregion

		#region Tree View

			#region Context Menu

		private void TreeViewContextMenu_AddToQueue(object sender, RoutedEventArgs e)
		{
			Category category = CategoryTreeView.SelectedItem as Category;
			AddCategoryToQueue(category);
		}

		private void TreeViewContextMenu_MoveDown(object sender, RoutedEventArgs e)
		{
			Category selectedCategory = CategoryTreeView.SelectedItem as Category;
			if (selectedCategory == null) return;
			if (selectedCategory != GlobalData.Library.Categories.Last())
			{
				int newIndex = GlobalData.Library.Categories.IndexOf(selectedCategory) + 1;
				GlobalData.Library.Categories.Remove(selectedCategory);
				GlobalData.Library.Categories.Insert(newIndex, selectedCategory);
			}
		}

		private void TreeViewContextMenu_MoveUp(object sender, RoutedEventArgs e)
		{
			Category selectedCategory = CategoryTreeView.SelectedItem as Category;
			if (selectedCategory == null) return;
			if (selectedCategory != GlobalData.Library.Categories.First())
			{
				int newIndex = GlobalData.Library.Categories.IndexOf(selectedCategory) - 1;
				GlobalData.Library.Categories.Remove(selectedCategory);
				GlobalData.Library.Categories.Insert(newIndex, selectedCategory);
			}
		}

		private void TreeViewContextMenu_Rename(object sender, RoutedEventArgs e)
		{
			Category category = CategoryTreeView.SelectedItem as Category;
			if (category != null)
			{
				InputDialog.Display("Rename category", "Category Name", category.Name, "Rename", VerifyCategoryName, RenameSelectedCategory);
			}
		}

		private void TreeViewContextMenu_Delete(object sender, RoutedEventArgs e)
		{
			DeleteSelectedCategory();
		}

			#endregion

			#region Mouse Events

		bool draggingTreeViewItem = false;

		private void CategoryTreeViewPreviewLeftMouseDown(object sender, MouseButtonEventArgs e)
		{
			// Find the original object that raised the event
			UIElement clickedItem = VisualTreeHelper.GetParent(e.OriginalSource as UIElement) as UIElement;

			// Find the clicked TreeViewItem
			while ((clickedItem != null) && !(clickedItem is TreeViewItem))
			{
				clickedItem = VisualTreeHelper.GetParent(clickedItem) as UIElement;
			}

			TreeViewItem treeViewItem = clickedItem as TreeViewItem;
			if (treeViewItem == null) return;
			Category category = treeViewItem.Header as Category;

			if (treeViewItem.Header is CategoryGroup)
			{
				e.Handled = true;
				return;
			}

			if (category != null)
			{
				GlobalData.Session.CurrentCategory = category;
				draggingTreeViewItem = true;
			}
			else if (treeViewItem.Header is AllSongs)
			{
				GlobalData.Session.ShowAllSongs();
			}
			else return;

            // Do a refresh
            RestoreFilteringAndSorting(GlobalData.Session.CurrentFilter, GlobalData.Session.CurrentSortDirective);
            CollectionViewSource.GetDefaultView(SongList.ItemsSource).Refresh();	
		}

		protected void CategoriesMouseLeave(object sender, MouseEventArgs e)
		{
			var source = e.OriginalSource as DependencyObject;

			while (source is ContentElement) source = LogicalTreeHelper.GetParent(source);
			while (source != null && !(source is TreeViewItem)) source = VisualTreeHelper.GetParent(source);

			var draggedItem = source as TreeViewItem;

			if (e.LeftButton == MouseButtonState.Pressed && draggedItem != null && draggingTreeViewItem)
			{
				Category category = draggedItem.Header as Category;

				// Initiate drag drop
				DataObject data = new DataObject("Category", category);
				DragDrop.DoDragDrop(draggedItem, data, DragDropEffects.Move);
			}

			draggingTreeViewItem = false;
		}

		private void CategoryTreeViewPreviewRightMouseDown(object sender, MouseButtonEventArgs e)
		{
			// Find the original object that raised the event
			UIElement clickedItem = VisualTreeHelper.GetParent(e.OriginalSource as UIElement) as UIElement;

			// Find the clicked TreeViewItem
			while ((clickedItem != null) && !(clickedItem is TreeViewItem))
			{
				clickedItem = VisualTreeHelper.GetParent(clickedItem) as UIElement;
			}

			var clickedTreeViewItem = clickedItem as TreeViewItem;
			if (clickedTreeViewItem == null) return;
			clickedTreeViewItem.IsSelected = true;
			clickedTreeViewItem.Focus();
		}

		
		private void CategoryTreeViewDoubleClicked(object sender, MouseButtonEventArgs e)
		{
			MusicPlayer.PlayFromCurrentCategory();
		}

			#endregion

			#region Drop Event

		void CategoryDropped(object sender, DragEventArgs e)
		{
			// Find the original object that raised the event
			UIElement targetItem = VisualTreeHelper.GetParent(e.OriginalSource as UIElement) as UIElement;

			// Find the clicked TreeViewItem
			while ((targetItem != null) && !(targetItem is TreeViewItem))
			{
				targetItem = VisualTreeHelper.GetParent(targetItem) as UIElement;
			}

			TreeViewItem treeViewItem = targetItem as TreeViewItem;
			Category targetCategory = treeViewItem.Header as Category;

			if (targetCategory == null) return;

			// Get the data being dropped
			List<SongData> droppedSongs = e.Data.GetData("Songs") as List<SongData>;
			Category droppedCategory = e.Data.GetData("Category") as Category;
			if (droppedSongs != null)
			{
				AddSongsToCategory(droppedSongs, targetCategory);
			}
			else if (droppedCategory != null)
			{
				int targetIndex = GlobalData.Library.Categories.IndexOf(targetCategory);
				GlobalData.Library.Categories.Remove(droppedCategory);
				GlobalData.Library.Categories.Insert(targetIndex, droppedCategory);

				GlobalData.SaveLibrary();

				// Select the dropped category
				TreeViewItem categories = CategoryTreeView.ItemContainerGenerator.ContainerFromIndex(1) as TreeViewItem;
				TreeViewItem droppedItem = categories.ItemContainerGenerator.ContainerFromItem(droppedCategory) as TreeViewItem;
				droppedItem.IsSelected = true;
			}
		}

			#endregion

		#endregion

		#region Column Splitters

		private void LeftSplitterMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (LeftColumn.ActualWidth < LeftColumnContent.MinWidth)
			{
				if (LeftColumn.ActualWidth < LeftColumnContent.MinWidth * 2D / 5D)
				{
					LeftColumn.Width = new GridLength(0);
				}
				else
				{
					LeftColumn.Width = new GridLength(LeftColumnContent.MinWidth);
				}
			}
		}

		private void RightSplitterMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (RightColumn.ActualWidth < RightColumnContent.MinWidth)
			{
				if (RightColumn.ActualWidth < RightColumnContent.MinWidth * 2D / 5D)
				{
					RightColumn.Width = new GridLength(0);
				}
				else
				{
					RightColumn.Width = new GridLength(RightColumnContent.MinWidth);
				}
			}
		}

		#endregion

		#region Thumbnail Button Events

		public void Thumnail_Play(object sender, EventArgs e)
        {
            MusicPlayer.Play();
        }

        public void Thumnail_Pause(object sender, EventArgs e)
        {
            MusicPlayer.Pause();
        }

        private void Thumnail_Previous(object sender, EventArgs e)
        {
            MusicPlayer.PlayPreviousSong();
        }

        private void Thumnail_Next(object sender, EventArgs e)
        {
            MusicPlayer.PlayNextSong();
        }

        #endregion
    }

	#region Tree View Data Classes

	public class CategoryGroup
	{
		public CategoryGroup()
		{
			Items = GlobalData.Library.Categories;
			Name = "Categories";
		}

		public String Name { get; set; }

		public ObservableCollection<Category> Items { get; set; }
	}

	public class AllSongs
	{
		public AllSongs()
		{
			Name = "All Songs";
		}

		public String Name { get; set; }
	}

	#endregion
}
