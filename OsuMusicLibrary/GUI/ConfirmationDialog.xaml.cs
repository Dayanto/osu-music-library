﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for ConfirmationDialog.xaml
	/// </summary>
	public partial class ConfirmationDialog : Window
	{
		public delegate void NextCall();

		private readonly String message;
		private readonly NextCall yesCall;
		private readonly NextCall noCall;

		public ConfirmationDialog(String title, String message, NextCall yesCall, NextCall noCall)
		{
			this.message = message;
			this.yesCall = yesCall;
			this.noCall = noCall;
			InitializeComponent();
			this.Title = title;
		}

		public static void Display(String title, String message, NextCall yes, NextCall no)
		{
			var confirmation = new ConfirmationDialog(title, message, yes, no);
			confirmation.Show();
		}

		private void Yes_Click(object sender, RoutedEventArgs e)
		{
			yesCall();
			this.Close();
		}

		private void No_Click(object sender, RoutedEventArgs e)
		{
			noCall();
			this.Close();
		}

		private void Textblock_Loaded(object sender, RoutedEventArgs e)
		{
			TextBlock textblock = sender as TextBlock;
			textblock.Text = message;
		}
	}
}
