﻿using System;
using System.Windows;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for InputDialog.xaml
	/// </summary>
	public partial class InputDialog : Window
	{
		public delegate bool Verify(String text);
		public delegate void Complete(String text);

		private readonly Verify verifyCall;
		private readonly Complete completeCall;

		public InputDialog(String title, String description, String defaultValue, String buttonText, Verify verifyCall, Complete completeCall)
		{
			this.verifyCall = verifyCall;
			this.completeCall = completeCall;
			InitializeComponent();
			Title = title;
			Description.Content = description;
			TextField.Text = defaultValue;
			ButtonText.Content = buttonText;
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			if (verifyCall(TextField.Text))
			{
				completeCall(TextField.Text);
				this.Close();
			}
		}

		public static void Display(String title, String description, String defaultValue, String buttonText, Verify verifyCall, Complete completeCall)
		{
			InputDialog window = new InputDialog(title, description, defaultValue, buttonText, verifyCall, completeCall);
			window.Show();
		}
	}
}
