﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using OsuMusicLibrary.Data;
using WinForms = System.Windows.Forms;
using OsuMusicLibrary.Media;

namespace OsuMusicLibrary.GUI
{
	/// <summary>
	/// Interaction logic for Settings.xaml
	/// </summary>
	public partial class SettingsWindow : Window
	{
		public SettingsWindow()
		{
			InitializeComponent();
            Settings settings = GlobalData.Settings;

			// Slider
			Volume.Value = GlobalData.Settings.Volume * Volume.Maximum;

            // Textbox
            FilePathBox.Text = GlobalData.Settings.OsuInstallPath;

            // Combobox
            ComboBoxNextSong.SelectedItem = settings.SwitchSongs ? PlayNew : (settings.AutoRepeat ? AutoRepeat : Stop);

            // Checkbox
            IgnorePreviewTime.IsChecked = GlobalData.Settings.IgnorePreviewTime;
			Unicode.IsChecked = GlobalData.Settings.PreferUnicode;
            Shuffle.IsChecked = GlobalData.Settings.Shuffle;
			AutoSynch.IsChecked = GlobalData.Settings.AutoSynch;
            ConfirmMulti.IsChecked = GlobalData.Settings.ConfirmMultiActions;
			ConfirmDelete.IsChecked = GlobalData.Settings.ConfirmDelete;

			// GroupBox
			VolumeBox.Header = "Volume (" + Math.Round(Volume.Value) + "%)";
		}

		private static SettingsWindow OpenWindow = null;

		internal static void Display()
		{
			if (OpenWindow == null)
			{
				SettingsWindow settingsWindow = new SettingsWindow();
				settingsWindow.Show();
				OpenWindow = settingsWindow;
			}
			else
			{
				OpenWindow.Focus();
				OpenWindow.BringIntoView();
			}
		}

		private void OnClose(object sender, EventArgs e)
		{
			OpenWindow = null;
		}

		private void OpenButton_Click(object sender, RoutedEventArgs e)
		{
			var dialog = new WinForms.FolderBrowserDialog();
			var result = dialog.ShowDialog();

			if (result == WinForms.DialogResult.OK)
			{
				String selectedPath = dialog.SelectedPath;

				// Attempt to update the install path
				if (GlobalData.Settings.TryUpdateOsuInstallPath(selectedPath))
				{
					// Update the textbox and save
					FilePathBox.Text = selectedPath;
					GlobalData.SaveSettings();
					GlobalData.CreateNewSongCache();
					GlobalData.SongCache.Synchronize();
					DialogBox.Display("Path updated", "Successfully updated the osu! installation path.");
				}
				else
				{
					// Path was invalid. Show a popup warning and keep the previous window open.
					DialogBox.Display("Invalid file path", "That path is not a valid osu! directory. Make sure that you didn't select any of the sub-directories.");
				}
			}
		}

        /**
		 * Update the song selection based on the current selection
		 */
        private void ComboBoxNextSong_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
			// Update the settings based on which item was selected
            if (ComboBoxNextSong.SelectedItem == Stop) { GlobalData.Settings.SwitchSongs = false; GlobalData.Settings.AutoRepeat = false; }
            if (ComboBoxNextSong.SelectedItem == AutoRepeat) { GlobalData.Settings.SwitchSongs = false; GlobalData.Settings.AutoRepeat = true; }
            if (ComboBoxNextSong.SelectedItem == PlayNew) { GlobalData.Settings.SwitchSongs = true; }
        }

		private void IgnorePreview_Changed(object sender, RoutedEventArgs e)
		{
			if (IgnorePreviewTime.IsChecked == null) return;

			GlobalData.Settings.IgnorePreviewTime = (bool)IgnorePreviewTime.IsChecked;
			GlobalData.SaveSettings();
		}

		private void Unicode_Changed(object sender, RoutedEventArgs e)
		{
			if (Unicode.IsChecked == null) return;

			GlobalData.Settings.PreferUnicode = (bool)Unicode.IsChecked;
			GlobalData.SaveSettings();
			CollectionViewSource.GetDefaultView(MainWindow.OMLWindow.SongList.ItemsSource).Refresh();
		}

		private void AutoSynch_Changed(object sender, RoutedEventArgs e)
		{
			if (AutoSynch.IsChecked == null) return;

			GlobalData.Settings.AutoSynch = (bool)AutoSynch.IsChecked;
			GlobalData.SaveSettings();

			if ((bool)AutoSynch.IsChecked)
				SongScanner.Start();
			else
				SongScanner.Stop();
		}

        private void Shuffle_Changed(object sender, RoutedEventArgs e)
		{
	        if (Shuffle.IsChecked == null) return;

	        GlobalData.Settings.Shuffle = (bool)Shuffle.IsChecked;
	        GlobalData.SaveSettings();
		}

        private void ConfirmMultipleActions_Changed(object sender, RoutedEventArgs e)
        {
	        if (ConfirmMulti.IsChecked == null) return;

	        GlobalData.Settings.ConfirmMultiActions = (bool)ConfirmMulti.IsChecked;
	        GlobalData.SaveSettings();
        }

		private void ConfirmDelete_Changed(object sender, RoutedEventArgs e)
		{
			if (ConfirmDelete.IsChecked == null) return;

			GlobalData.Settings.ConfirmDelete = (bool)ConfirmDelete.IsChecked;
			GlobalData.SaveSettings();
		}

		private void VolumeChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			GlobalData.Settings.Volume = MusicPlayer.Player.Volume = Volume.Value / Volume.Maximum;
			GlobalData.SaveSettings();

			// Update the header
			VolumeBox.Header = "Volume (" + Math.Round(Volume.Value) + "%)";
		}
	}
}
