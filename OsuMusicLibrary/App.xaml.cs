﻿using System;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using OsuMusicLibrary.Data;
using OsuMusicLibrary.GUI;
using OsuMusicLibrary.Media;
using OsuMusicLibrary.Program;
using OsuMusicLibrary.Util;

namespace OsuMusicLibrary
{
	/// <summary> StartupUri="GUI/MainWindow.xaml"
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static Application Instance { get; private set; }
		public static bool Initialized = false;

        // Keyboard listener used to listen for media keys
		private static readonly KeyboardListener KListener = new KeyboardListener();

		void App_Startup(object sender, StartupEventArgs e)
		{
			Instance = this;

			// Make sure there aren't any other instances running
			if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
            {
                MessageBox.Show("Application already running. Only one instance of this application is allowed");
                Shutdown();
	            return;
            }

#if DEBUG
			// Enable the console
			ConsoleManager.Show();
#endif
            // Add a global hook to listen for keyboard presses
            KListener.KeyUp += new RawKeyEventHandler(KListener_KeyUp);

			// Make sure the program has write access
			if (!HasWriteAccessToFolder("/"))
			{
				DialogBox.Display("Access denied", "The program doesn't have write access to the current directory. Either move the program or give it administrator rights.");
				return;
			}

            // Add an exception handler in case any exceptions slip through 
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CrashHandler);

			Initializer.Run();
		}

		private void App_Exit(object sender, ExitEventArgs e)
		{
            // Remove the global hook
            KListener.Dispose();

			if (Initialized)
			{
				GlobalData.SaveSettings();
				GlobalData.SaveLibrary();
			}
		}

        private static void CrashHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            Logger.LogException(e);
        }

        public static bool HasWriteAccessToFolder(string folderPath)
        {
            try
            {
                // Attempt to save a file to see if we have write access
                const string filepath = "___WRITE_ACCESS_TEST___.txt";
                File.WriteAllText(filepath, "File used to test write access. It can safely be deleted.");
                File.Delete(filepath);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (IOException)
            {
                return false;
            }
            catch (NotSupportedException)
            {
                return false;
            }
            catch (SecurityException)
            {
                return false;
            }
        }

		private static void KListener_KeyUp(object sender, RawKeyEventArgs args)
        {
            if (GUI.MainWindow.OMLWindow.IsInitialized)
            {
				Console.WriteLine(args.Key);
                if (args.Key == Key.MediaPlayPause)
                {
                    UIDispatcher.BeginInvoke((Action)(() => { (MusicPlayer.Playing ? (Action)MusicPlayer.Pause : MusicPlayer.Play)(); }));
                }
                if (args.Key == Key.MediaNextTrack)
                {
                    UIDispatcher.BeginInvoke((Action)MusicPlayer.PlayNextSong);
                }
                if (args.Key == Key.MediaPreviousTrack)
                {
					UIDispatcher.BeginInvoke((Action)MusicPlayer.PlayPreviousSong);
                }
                if (args.Key == Key.MediaStop)
                {
					UIDispatcher.BeginInvoke((Action)MusicPlayer.Stop);
                }
            }
            else
            {
                Console.WriteLine("NotInitialized");
            }
        }

        private static void BeginInvoke(Action action)
        {
            UIDispatcher.BeginInvoke(action);
        }

		public static void SetShutdownMode(ShutdownMode shutdownMode)
		{
			Instance.ShutdownMode = shutdownMode;
		}

		public static void Close()
		{
			Instance.Shutdown();
		}

		public static Dispatcher UIDispatcher
		{
			get { return Instance.Dispatcher; }
		}
	}
}
